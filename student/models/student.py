from django.db import models

SEX_CHOICES = (
    ('male', 'Male'),
    ('female', 'Female'),
    ('other', 'Other')
)
FATHER_STATUS = (
    ('living', 'Living'),
    ('deceased', 'Deceased'),
    ('deserted', 'Deserted')
)

GUARDIAN_TYPE = (
    ('mother', 'Mother'),
    ('father', 'Father'),
    ('other', 'Other')

)
GENDERS = (('male', 'Male'),
           ('female', 'Female'), ('other', 'Other'))


TEST_STATUSES = (
    ('pending', 'Pending'),
    ('passed', 'Passed'),
    ('failed', 'Failed'),
    ('partial', 'Partial')
)
LEARNING_STATUSES = (
    ('pending', 'Pending'),
    ('passed', 'Passed'),
    ('failed', 'Failed'),
)


class Vehicle(models.Model):
    driving_school = models.ForeignKey(
        'utilities.DrivingSchool', on_delete=models.CASCADE,
        related_name='vehicles', null=True, blank=True)
    vehicle_type = models.CharField(max_length=100)

    def __str__(self):
        return self.vehicle_type


class RTO(models.Model):
    driving_school = models.ForeignKey(
        'utilities.DrivingSchool', on_delete=models.CASCADE,
        related_name='rtos', null=True, blank=True)
    rto_name = models.CharField(max_length=100)
    rto_number = models.IntegerField()

    def __str__(self):
        return f'{self.rto_name}-{self.rto_number}'


class Student(models.Model):
    driving_school = models.ForeignKey(
        'utilities.DrivingSchool', on_delete=models.CASCADE)
    user = models.ForeignKey(
        'user_auth.User', on_delete=models.CASCADE, related_name='students')
    application_number = models.CharField(max_length=32, null=True, blank=True)
    name = models.CharField(max_length=100)
    gender = models.CharField(
        max_length=10, choices=GENDERS, null=True, blank=True)
    mobile_number = models.CharField(max_length=12)
    whatsapp_number = models.CharField(max_length=12, blank=True, null=True)
    alternate_mobile_number = models.CharField(
        max_length=12, blank=True, null=True)
    aadhaar_number = models.CharField(max_length=12, blank=True, null=True)
    license_number = models.CharField(max_length=20, blank=True, null=True)
    dob = models.DateField(blank=True, null=True)
    blood_group = models.CharField(max_length=5, blank=True, null=True)
    qualification = models.CharField(max_length=100, blank=True, null=True)
    state = models.CharField(max_length=100, blank=True, null=True)
    district = models.CharField(max_length=100, blank=True, null=True)
    taluk = models.CharField(max_length=100, blank=True, null=True)
    village = models.CharField(max_length=100, blank=True, null=True)
    address = models.CharField(max_length=100, blank=True, null=True)
    pin_code = models.CharField(max_length=6, blank=True, null=True)
    temporary_address = models.CharField(max_length=100, blank=True, null=True)
    class_of_vehicle = models.ManyToManyField(
        Vehicle, blank=True, related_name='students')
    identification_mark_1 = models.CharField(
        max_length=100, blank=True, null=True)
    identification_mark_2 = models.CharField(
        max_length=100, blank=True, null=True)
    rto = models.ForeignKey(
        RTO, on_delete=models.SET_NULL, null=True, blank=True)
    is_verified = models.BooleanField(default=False)
    is_rejected = models.BooleanField(default=False)
    image = models.URLField(blank=True, null=True)
    request_date = models.DateField(auto_now_add=True)
    total_fees = models.FloatField(default=0, blank=True, null=True)
    passed_date = models.DateField(blank=True, null=True)
    rto_class_date = models.DateField(blank=True, null=True)

    def __str__(self):
        return self.name


class StudentFile(models.Model):
    student = models.ForeignKey(
        Student, on_delete=models.CASCADE, related_name='files')
    proof_file = models.URLField()
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.student.name


class StudentClass(models.Model):
    student = models.ForeignKey(
        Student, on_delete=models.CASCADE, related_name='classes')
    date = models.DateField(null=True, blank=True)
    is_attended = models.BooleanField(default=False)
    description = models.CharField(max_length=100, blank=True, null=True)
    additional_fees = models.FloatField(default=0, blank=True, null=True)

    def __str__(self):
        return self.student.name


class DrivingTest(models.Model):
    student = models.ForeignKey(
        Student, on_delete=models.CASCADE, related_name='tests')
    date = models.DateField(null=True, blank=True)
    description = models.CharField(max_length=100, blank=True, null=True)
    status = models.CharField(max_length=100, choices=TEST_STATUSES)
    additional_fee = models.FloatField(default=0, blank=True, null=True)

    def __str__(self):
        return self.student.name


class LearningTest(models.Model):
    student = models.ForeignKey(
        Student, on_delete=models.CASCADE, related_name='learning_tests')
    date = models.DateField(null=True, blank=True)
    description = models.CharField(max_length=100, blank=True, null=True)
    status = models.CharField(max_length=100, choices=LEARNING_STATUSES)
    additional_fee = models.FloatField(default=0, blank=True, null=True)

    def __str__(self):
        return self.student.name

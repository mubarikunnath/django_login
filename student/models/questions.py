from django.db import models


class Question(models.Model):
    question = models.TextField()
    answer = models.TextField(null=True, blank=True)
    answer_file = models.URLField(null=True, blank=True)
    answer_type = models.CharField(
        choices=(('text', 'Text'), ('file', 'File')), max_length=32)
    created_at = models.DateTimeField(auto_now_add=True)
    country = models.ForeignKey('utilities.Country', on_delete=models.CASCADE)

# from coreapi import Field
# from rest_framework.filters import BaseFilterBackend


# class StudentTestFilterBackend(BaseFilterBackend):
#     def get_schema_fields(self, view):
#         return [
#             Field(
#                 name='test_completed',
#                 location='query',
#                 required=False,
#                 type='string'
#             ),
#         ]

#     def filter_queryset(self, request, queryset, view):
#         test_completed = request.query_params.get('test_completed', None)

#         if test_completed:
#             queryset = queryset.filter(test_completed=test_completed)
#         return queryset


# class MonthYearFilterBackendTwo(BaseFilterBackend):
#     def get_schema_fields(self, view):
#         return [
#             Field(
#                 name='month',
#                 location='query',
#                 required=False,
#                 type='string'
#             ),
#             Field(
#                 name='year',
#                 location='query',
#                 required=False,
#                 type='string'
#             ),
#         ]

#     def filter_queryset(self, request, queryset, view):
#         month = request.query_params.get('month', None)
#         year = request.query_params.get('year', None)

#         return queryset


# class StudentFilterBackend(BaseFilterBackend):
#     def get_schema_fields(self, view):
#         return [
#             Field(
#                 name='status',
#                 location='query',
#                 required=False,
#                 type='string'
#             ),
#             Field(
#                 name='on_leave',
#                 location='query',
#                 required=False,
#                 type='string'
#             ),

#         ]

#     def filter_queryset(self, request, queryset, view):
#         status = request.query_params.get('status', None)
#         on_leave = request.query_params.get('on_leave', None)
#         if status == 'present':
#             queryset = queryset.filter(
#                 studentadmission__date_of_leaving__isnull=True)
#         elif status == 'left':
#             queryset = queryset.filter(
#                 studentadmission__date_of_leaving__isnull=False)
#         if on_leave:
#             queryset = queryset.filter(
#                 absents__date=date.today())

#         return queryset

from rest_framework import serializers
from student.models import RTO


class RTOSerializer(serializers.ModelSerializer):
    class Meta:
        model = RTO
        exclude = ('driving_school',)

    def create(self, validated_data):
        validated_data['driving_school'] = self.context.get(
            'user').driving_school
        return super().create(validated_data)

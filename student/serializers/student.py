from rest_framework import serializers
from django.shortcuts import get_object_or_404
from utilities.serializers import ChequeDetailsSerializer
from django.db import transaction as atomic_transaction
from utilities.models import (
    Transaction,
    ChequeDetails
)
from student.models import (
    Student,
    StudentFile,
    Vehicle,
    StudentClass,
    DrivingTest,
    LearningTest

)
from datetime import date
from user_auth.models import User


class StudentRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        exclude = ('is_verified', 'application_number', 'rto', 'is_rejected',
                   'user', 'rto_class_date', 'passed_date', 'total_fees')


class StudentRequestListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ('id', 'name', 'mobile_number', 'whatsapp_number', 'address')


class StudentListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ('id', 'application_number', 'name',
                  'mobile_number', 'whatsapp_number')

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        if instance.tests.filter(
                status='pending', date__gte=date.today()).exists():
            test = instance.tests.filter(
                status='pending', date__gte=date.today()).order_by('date')[0]
            representation['next_test_date'] = test.date
        if instance.tests.exists():
            status = instance.tests.order_by('date').last().status
            representation['status'] = status
        if instance.passed_date:
            representation['status'] = 'passed'
        else:
            if instance.tests.filter(status='passed').exists():
                representation['status'] = 'passed'
            elif instance.tests.filter(status='partial').exists():
                representation['status'] = 'Partially Passed'
            elif instance.tests.filter(status='failed').exists():
                representation['status'] = 'Failed'
            else:
                representation['status'] = 'Pending'
        return representation


class StudentDetailSerializer(serializers.ModelSerializer):
    number_of_classes = serializers.IntegerField(
        write_only=True, required=False)

    class Meta:
        model = Student
        exclude = ('driving_school', 'user')

    def update(self, instance, validated_data):
        number_of_classes = None
        if validated_data.get('number_of_classes'):
            number_of_classes = validated_data.pop('number_of_classes')

        instance = super().update(instance, validated_data)
        if instance.classes.count() == 0 and number_of_classes:
            for i in range(number_of_classes):
                StudentClass.objects.create(student=instance)
        return instance

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['class_of_vehicle'] = instance.class_of_vehicle.values(
            'id', 'vehicle_type')
        data['number_of_classes'] = instance.classes.count()
        data['total_paid_amount'] = sum(
            list(instance.transactions.filter(type=1).values_list('amount', flat=True)))

        return data


class StudentCreateSerializer(serializers.ModelSerializer):
    number_of_classes = serializers.IntegerField(required=False)

    class Meta:
        model = Student
        exclude = ('user', 'driving_school')

    def create(self, validated_data):
        validated_data['driving_school'] = self.context.get(
            'user').driving_school
        number_of_classes = validated_data.pop('number_of_classes', None)
        if User.objects.filter(
                username=validated_data['mobile_number']).exists():
            user = User.objects.get(
                username=validated_data['mobile_number'])
        else:
            user = User.objects.create(
                mobile=validated_data['mobile_number'],
                username=validated_data['mobile_number'],
                name=validated_data['name'],
                user_type=5
            )
        validated_data['user'] = user
        student = super().create(validated_data)
        classes = []
        if number_of_classes:
            for i in range(number_of_classes):
                classes.append(StudentClass(student=student))
        if classes:
            StudentClass.objects.bulk_create(classes)
        return student


class VehicleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vehicle
        fields = '__all__'

    def create(self, validated_data):
        validated_data['driving_school'] = self.context.get(
            'user').driving_school

        return super().create(validated_data)


class StudentFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentFile
        fields = ('id', 'proof_file', 'title', 'description')

    def create(self, validated_data):
        validated_data['student'] = get_object_or_404(
            Student, pk=self.context.get('student'))
        return super().create(validated_data)


class StudentPaymentSerializer(serializers.ModelSerializer):
    cheque_details = ChequeDetailsSerializer(required=False)
    receipt_id = serializers.CharField(read_only=True)

    class Meta:
        model = Transaction
        exclude = ('creator', 'student', 'is_salary', 'salary_date',
                   'staff', 'is_student_transaction')

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        if validated_data.get('category'):
            if validated_data.get('category').type != validated_data.get('type'):
                raise serializers.ValidationError(
                    'category type does not match')
        if validated_data['mode_of_payment'] == 3:
            if validated_data['bank_account'] is None:
                raise serializers.ValidationError(
                    {'bank_account': 'This field is required.'})
        elif validated_data['mode_of_payment'] == 2:
            if validated_data['cheque_details'] is None:
                raise serializers.ValidationError(
                    {'cheque_details': 'This field is required.'})
        self.student = get_object_or_404(
            Student, pk=self.context.get('student'))
        paid_amount = sum(list(self.student.transactions.filter(
            type=1).values_list('amount', flat=True)))
        if not self.instance:
            if paid_amount + validated_data['amount'] > self.student.total_fees:
                raise serializers.ValidationError(
                    'Amount cannot be greater than total fees')
            return validated_data
        else:
            if paid_amount - self.instance.amount + validated_data['amount'] > self.student.total_fees:
                raise serializers.ValidationError(
                    'Amount cannot be greater than total fees')
            return validated_data

    def create(self, validated_data):
        validated_data['driving_school'] = self.context.get(
            'user').driving_school
        validated_data['student'] = self.student
        validated_data['is_student_transaction'] = True
        validated_data['creator'] = self.context.get('user')
        if validated_data.get('cheque_details'):
            cheque_details_data = validated_data.pop('cheque_details')
            cheque_details = ChequeDetails.objects.create(
                **cheque_details_data)
            validated_data['cheque_details'] = cheque_details
        return super().create(validated_data)

    def update(self, instance, validated_data):
        with atomic_transaction.atomic():
            instance.delete()
            validated_data['driving_school'] = self.context.get(
                'user').driving_school
            validated_data['student'] = get_object_or_404(
                Student, pk=self.context.get('student'))
            validated_data['is_student_transaction'] = True
            validated_data['creator'] = self.context.get('user')
            if validated_data.get('cheque_details'):
                cheque_details_data = validated_data.pop('cheque_details')
                cheque_details = ChequeDetails.objects.create(
                    **cheque_details_data)
                validated_data['cheque_details'] = cheque_details

            instance = Transaction.objects.create(**validated_data)
        return instance

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.category:
            data['category_name'] = instance.category.title

        if instance.sub_category:
            data['sub_category_name'] = instance.sub_category.title
        data['display_date'] = instance.date.strftime('%d-%m-%Y')
        if instance.salary_date:
            data['display_salary_date'] = instance.salary_date.strftime(
                '%d-%m-%Y')
        if instance.creator:
            data['creator_name'] = instance.creator.name
        return data


class StudentUpcomingDrivingTestSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='student.name')
    mobile_number = serializers.CharField(source='student.mobile_number')
    rto = serializers.SerializerMethodField()
    vehicles = serializers.SerializerMethodField()

    class Meta:
        model = DrivingTest
        fields = ('id', 'name', 'rto', 'mobile_number', 'date', 'vehicles')

    def get_rto(self, obj):
        return obj.student.rto.rto_name if obj.student.rto else None

    def get_vehicles(self, obj):
        return obj.student.class_of_vehicle.values('id', 'vehicle_type')


class StudentUpcomingLearningTestSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='student.name')
    mobile_number = serializers.CharField(source='student.mobile_number')
    rto = serializers.SerializerMethodField()

    class Meta:
        model = LearningTest
        fields = ('id', 'name', 'rto', 'mobile_number', 'date')

    def get_rto(self, obj):
        return obj.student.rto.rto_name if obj.student.rto else None


class StudentUpcomingRtoClassSerializer(serializers.ModelSerializer):
    rto = serializers.SerializerMethodField()

    class Meta:
        model = Student
        fields = ('id', 'name', 'rto', 'mobile_number', 'rto_class_date')

    def get_rto(self, obj):
        return obj.rto.rto_name if obj.rto else None


class StudentClassSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentClass
        exclude = ('student',)

    def create(self, validated_data):
        validated_data['student'] = get_object_or_404(
            Student, pk=self.context.get('student_id'))
        student_class = super().create(validated_data)
        if student_class.additional_fees > 0:
            student_class.student.total_fees += student_class.additional_fees
            student_class.student.save()

        return student_class

    def update(self, instance, validated_data):
        if instance.additional_fees > 0:
            instance.student.total_fees -= instance.additional_fees
            instance.student.save()
        instance = super().update(instance, validated_data)
        if instance.additional_fees > 0:
            instance.student.total_fees += instance.additional_fees
            instance.student.save()
        return instance


class DrivingTestSerializer(serializers.ModelSerializer):
    class Meta:
        model = DrivingTest
        exclude = ('student',)

    def create(self, validated_data):
        validated_data['student'] = get_object_or_404(
            Student, pk=self.context.get('student_id'))
        driving_test = super().create(validated_data)
        if driving_test.additional_fee > 0:
            driving_test.student.total_fees += driving_test.additional_fee
            driving_test.student.save()
        return driving_test

    def update(self, instance, validated_data):
        if instance.additional_fee > 0:
            instance.student.total_fees -= instance.additional_fee
            instance.student.save()
        instance = super().update(instance, validated_data)
        if instance.additional_fee > 0:
            instance.student.total_fees += instance.additional_fee
            instance.student.save()
        return instance


class LearningTestSerializer(serializers.ModelSerializer):
    class Meta:
        model = LearningTest
        exclude = ('student',)

    def create(self, validated_data):
        validated_data['student'] = get_object_or_404(
            Student, pk=self.context.get('student_id'))
        learning_test = super().create(validated_data)
        if learning_test.additional_fee > 0:
            learning_test.student.total_fees += learning_test.additional_fee
            learning_test.student.save()
        return learning_test

    def update(self, instance, validated_data):
        if instance.additional_fee > 0:
            instance.student.total_fees -= instance.additional_fee
            instance.student.save()
        instance = super().update(instance, validated_data)
        if instance.additional_fee > 0:
            instance.student.total_fees += instance.additional_fee
            instance.student.save()
        return instance

from django.contrib import admin

# Register your models here.
from student.models import (
    Student,
    StudentFile,
    Vehicle,
    RTO,
    StudentClass,
    DrivingTest,
    LearningTest,
    Question,
)


class StudentAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)


admin.site.register(Student, StudentAdmin)
admin.site.register(StudentFile)
admin.site.register(Vehicle)
admin.site.register(RTO)
admin.site.register(StudentClass)
admin.site.register(DrivingTest)
admin.site.register(LearningTest)
admin.site.register(Question)

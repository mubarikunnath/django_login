from user_auth.permission import (
    CustomListCreateAPIView,
    CustomRetrieveUpdateDestroyAPIView,
)
from drivezy.response import SuccessResponse

from user_auth.permission import IsAuthenticatedUser

from student.models import RTO
from student.serializers import RTOSerializer


class RTOListCreateView(CustomListCreateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = RTOSerializer

    def get_queryset(self):
        return RTO.objects.filter(
            driving_school=self.request.user.driving_school)

    def get_serializer_context(self):
        return {'user': self.request.user}


class RTORetrieveUpdateDestroyView(CustomRetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = RTOSerializer
    allowed_methods = ('GET', 'PATCH', 'DELETE')

    def get_queryset(self):
        return RTO.objects.filter(
            driving_school=self.request.user.driving_school)

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Successfully deleted RTO')

from user_auth.permission import (
    CustomCreateAPIView,
    CustomListAPIView,
    CustomListCreateAPIView,
    CustomRetrieveUpdateDestroyAPIView,
    CustomDestroyAPIView
)
from rest_framework.generics import (
    CreateAPIView
)
from user_auth.permission import IsAuthenticatedUser
from student.models import (
    Student,
    StudentFile,
    Vehicle,
    StudentClass,
    DrivingTest,
    LearningTest
)
from student.serializers import (
    StudentRequestSerializer,
    StudentRequestListSerializer,
    StudentListSerializer,
    StudentDetailSerializer,
    VehicleSerializer,
    StudentFileSerializer,
    StudentPaymentSerializer,
    StudentCreateSerializer,
    StudentUpcomingDrivingTestSerializer,
    StudentUpcomingLearningTestSerializer,
    StudentUpcomingRtoClassSerializer,
    StudentClassSerializer,
    DrivingTestSerializer,
    LearningTestSerializer,
)

from utilities.models import Transaction
from drivezy.response import SuccessResponse
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend
import datetime


class PublicStudentCreateView(CreateAPIView):
    '''
    public api for add student details
'''
    serializer_class = StudentRequestSerializer


class RequestStudentListView(CustomListAPIView):
    serializer_class = StudentRequestListSerializer
    permission_classes = (IsAuthenticatedUser,)
    queryset = Student.objects.filter(is_verified=False, is_rejected=False)
    filter_backends = [filters.SearchFilter]
    search_fields = ['name', ]

    def get_queryset(self):
        return super().get_queryset().filter(
            driving_school=self.request.user.driving_school)


class StudentListView(CustomListAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = StudentListSerializer
    queryset = Student.objects.prefetch_related(
        'tests').filter(is_verified=True)
    filter_backends = [filters.SearchFilter, ]
    search_fields = ['name', 'application_number']

    def get_queryset(self):
        return super().get_queryset().filter(
            driving_school=self.request.user.driving_school)


class StudentDetailView(CustomRetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = StudentDetailSerializer
    queryset = Student.objects.all()
    allowed_methods = ('GET', 'PATCH', 'DELETE')


class VehicleListCreateView(CustomListCreateAPIView):
    serializer_class = VehicleSerializer
    permission_classes = (IsAuthenticatedUser,)
    queryset = Vehicle.objects.all()

    def get_queryset(self):
        return super().get_queryset().filter(
            driving_school=self.request.user.driving_school)

    def get_serializer_context(self):
        return {'user': self.request.user}


class VehicleDetailView(CustomRetrieveUpdateDestroyAPIView):
    serializer_class = VehicleSerializer
    permission_classes = (IsAuthenticatedUser,)
    queryset = Vehicle.objects.all()
    allowed_methods = ('PATCH', 'DELETE')

    def get_queryset(self):
        return super().get_queryset(
        ).filter(driving_school=self.request.user.driving_school)



class StudentFileCreateView(CustomListCreateAPIView):
    serializer_class = StudentFileSerializer
    permission_classes = (IsAuthenticatedUser,)
    queryset = StudentFile.objects.all()

    def get_queryset(self):
        return super().get_queryset().filter(
            student__id=self.kwargs.get('pk'))

    def get_serializer_context(self):
        return {'student': self.kwargs.get('pk')}


class StudentFileDeleteView(CustomDestroyAPIView):
    serializer_class = StudentFileSerializer
    permission_classes = (IsAuthenticatedUser,)

    def get_queryset(self):
        return StudentFile.objects.filter(student=self.kwargs.get('std_id'))


class StudentCreateView(CustomCreateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = StudentCreateSerializer

    def get_serializer_context(self):
        return {'user': self.request.user}


class StudentPaymentListCreateView(CustomListCreateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = StudentPaymentSerializer

    def get_serializer_context(self):
        return {'student': self.kwargs.get('pk'),
                'user': self.request.user}

    def get_queryset(self):
        return Transaction.objects.filter(
            student__id=self.kwargs.get('pk')).order_by('-date')


class StudentPaymentUpdateDeleteView(CustomRetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = StudentPaymentSerializer
    allowed_methods = ('GET', 'PATCH', 'DELETE')

    def get_queryset(self):
        return Transaction.objects.filter(
            student__id=self.kwargs.get('student_id'))

    def get_serializer_context(self):
        return {'student': self.kwargs.get('student_id'),
                'user': self.request.user}

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Success')


class StudentUpcomingDrivingTestView(CustomListAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = StudentUpcomingDrivingTestSerializer
    queryset = DrivingTest.objects.filter(
        date__gte=datetime.date.today(), status='pending').order_by('date')
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    filterset_fields = ['date', ]
    search_fields = ['student__name', ]

    def get_queryset(self):
        return super().get_queryset(
        ).filter(student__driving_school=self.request.user.driving_school)


class StudentUpcomingLearningTestView(CustomListAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = StudentUpcomingLearningTestSerializer
    queryset = LearningTest.objects.filter(
        date__gte=datetime.date.today(), status='pending').order_by('date')
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    filterset_fields = ['date', ]
    search_fields = ['student__name', ]

    def get_queryset(self):
        return super().get_queryset(
        ).filter(student__driving_school=self.request.user.driving_school)


class StudentUpcomingRtoClassView(CustomListAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = StudentUpcomingRtoClassSerializer
    queryset = Student.objects.filter(
        rto_class_date__gte=datetime.date.today()).order_by('rto_class_date')
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    filterset_fields = ['rto_class_date', ]
    search_fields = ['name', ]

    def get_queryset(self):
        return super().get_queryset(
        ).filter(student__driving_school=self.request.user.driving_school)


class StudentClassListCreateView(CustomListCreateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = StudentClassSerializer

    def get_queryset(self):
        return StudentClass.objects.filter(
            student__id=self.kwargs.get('pk')).order_by('-date')

    def get_serializer_context(self):
        return {'student_id': self.kwargs.get('pk')}


class StudentClassUpdateDeleteView(CustomRetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = StudentClassSerializer
    allowed_methods = ('GET', 'PATCH', 'DELETE')

    def get_queryset(self):
        return StudentClass.objects.filter(
            student__id=self.kwargs.get('student_id'))

    def get_serializer_context(self):
        return {'student_id': self.kwargs.get('student_id')}

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.additional_fees > 0:
            instance.student.total_fees -= instance.additional_fees
            instance.student.save()
        instance.delete()
        return SuccessResponse(message='Success')


class DrivingTestListCreateView(CustomListCreateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = DrivingTestSerializer

    def get_queryset(self):
        return DrivingTest.objects.filter(
            student__id=self.kwargs.get('pk')).order_by('-date')

    def get_serializer_context(self):
        return {'student_id': self.kwargs.get('pk')}


class DrivingTestUpdateDeleteView(CustomRetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = DrivingTestSerializer
    allowed_methods = ('GET', 'PATCH', 'DELETE')

    def get_queryset(self):
        return DrivingTest.objects.filter(
            student__id=self.kwargs.get('student_id'))

    def get_serializer_context(self):
        return {'student_id': self.kwargs.get('student_id')}

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.additional_fee > 0:
            instance.student.total_fees -= instance.additional_fee
            instance.student.save()
        instance.delete()
        return SuccessResponse(message='Success')


class LearningTestListCreateView(CustomListCreateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = LearningTestSerializer

    def get_queryset(self):
        return LearningTest.objects.filter(
            student__id=self.kwargs.get('pk')).order_by('-date')

    def get_serializer_context(self):
        return {'student_id': self.kwargs.get('pk')}


class LearningTestUpdateDeleteView(CustomRetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = LearningTestSerializer
    allowed_methods = ('GET', 'PATCH', 'DELETE')

    def get_queryset(self):
        return LearningTest.objects.filter(
            student__id=self.kwargs.get('student_id'))

    def get_serializer_context(self):
        return {'student_id': self.kwargs.get('student_id')}

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.additional_fee > 0:
            instance.student.total_fees -= instance.additional_fee
            instance.student.save()
        instance.delete()
        return SuccessResponse(message='Success')

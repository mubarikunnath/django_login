

from kombu import Queue
from constants import DEFAULT_CELERY_QUEUE

# Celery
BROKER_URL = 'redis://redis:6379/0'
CELERY_RESULT_BACKEND = 'redis://redis:6379/0'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_DEFAULT_QUEUE = DEFAULT_CELERY_QUEUE
CELERY_TRACK_STARTED = True
CELERY_QUEUES = (
    Queue(DEFAULT_CELERY_QUEUE, routing_key='task.#'),
)
CELERY_TIMEZONE = "Asia/Kolkata"

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://redis:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}

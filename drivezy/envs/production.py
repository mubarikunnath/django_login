from decouple import config

print('Mode: Production')


STATIC_URL = config('PRE_URL')+'api/static/'
MEDIA_URL = config('PRE_URL')+'api/media/'
STATIC_ROOT = '/var/www/'+config('PRE_URL')+'api/static/'


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': config('DATABASE_NAME'),
        'USER': config('DB_USER'),
        'PASSWORD': config('DB_PASSWORD'),
        'HOST': 'localhost',
        'PORT': '',
    }
}

# Push notification settings
# PUSH_NOTIFICATIONS_SETTINGS = {
#     "FCM_API_KEY": config('FCM_API_KEY'),

# }

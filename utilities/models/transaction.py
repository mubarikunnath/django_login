from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_save
from student.models import Student

MODE_OF_PAYMENTS = (
    (1, 'Cash'),
    (2, 'Cheque'),
    (3, 'Bank Transfer'),
)


class BankAccount(models.Model):
    driving_school = models.ForeignKey(
        'utilities.DrivingSchool',
        on_delete=models.CASCADE, related_name='bank_accounts')
    account_number = models.CharField(max_length=18, unique=True)
    account_name = models.CharField(max_length=50)
    account_balance = models.FloatField(default=0)
    upi = models.CharField(max_length=50)
    ifsc = models.CharField(max_length=50)
    bank_name = models.CharField(max_length=50)
    bank_branch = models.CharField(max_length=50)
    bank_address = models.CharField(max_length=50, default='NA')
    is_primary = models.BooleanField(default=False)

    def __str__(self):
        return self.account_name


@receiver(post_save, sender=BankAccount)
def make_primary_bank_account(sender, instance, created, **kwargs):
    if instance.is_primary:
        BankAccount.objects.exclude(pk=instance.pk).update(is_primary=False)


class ChequeDetails(models.Model):
    driving_school = models.ForeignKey(
        'utilities.DrivingSchool',
        on_delete=models.CASCADE, related_name='cheque_details')
    cheque_number = models.CharField(max_length=50)
    cheque_date = models.DateField()
    cheque_amount = models.FloatField(default=0)
    cheque_status = models.BooleanField(default=False)
    cheque_remarks = models.CharField(max_length=50, default='NA')

    class Meta:
        verbose_name_plural = 'Cheque Details'

    def __str__(self):
        return self.cheque_number


class TransactionCategory(models.Model):
    driving_school = models.ForeignKey(
        'utilities.DrivingSchool',
        on_delete=models.CASCADE, related_name='transaction_categories')
    title = models.CharField(max_length=32, unique=True)
    type = models.PositiveSmallIntegerField(
        default=1, choices=((1, 'Income'), (2, 'Expense')))

    class Meta:
        verbose_name_plural = 'Transaction Categories'

    def __str__(self):
        return self.title


class TransactionSubCategory(models.Model):
    title = models.CharField(max_length=32, unique=True)
    category = models.ForeignKey(
        TransactionCategory,
        on_delete=models.CASCADE, related_name='sub_categories')

    class Meta:
        verbose_name_plural = 'Transaction Sub Categories'

    def __str__(self):
        return self.title


class Transaction(models.Model):
    driving_school = models.ForeignKey(
        'utilities.DrivingSchool',
        on_delete=models.CASCADE, related_name='transactions')
    creator = models.ForeignKey(
        'user_auth.User', on_delete=models.SET_NULL,
        null=True, blank=True)
    amount = models.FloatField(default=0)
    date = models.DateField()
    description = models.TextField(null=True, blank=True, default='')
    category = models.ForeignKey(
        TransactionCategory, on_delete=models.SET_NULL,
        null=True, blank=True)
    sub_category = models.ForeignKey(
        TransactionSubCategory, on_delete=models.SET_NULL,
        null=True, blank=True)
    type = models.PositiveSmallIntegerField(
        choices=((1, 'Income'), (2, 'Expense')))
    mode_of_payment = models.PositiveSmallIntegerField(
        choices=MODE_OF_PAYMENTS)
    bank_account = models.ForeignKey(
        BankAccount, on_delete=models.SET_NULL, null=True, blank=True)
    cheque_details = models.ForeignKey(
        ChequeDetails, on_delete=models.SET_NULL, null=True, blank=True)
    is_salary = models.BooleanField(default=False)
    attachment = models.URLField(null=True, blank=True)
    salary_date = models.DateField(null=True, blank=True)
    receipt_number = models.CharField(max_length=50, null=True, blank=True)
    staff = models.ForeignKey(
        'user_auth.User', on_delete=models.SET_NULL,
        null=True, blank=True, related_name='salaries')
    student = models.ForeignKey(
        Student, on_delete=models.SET_NULL, null=True,
        blank=True, related_name='transactions')
    is_student_transaction = models.BooleanField(default=False)

    @property
    def receipt_id(self):
        return 'DZ' + str(self.id).zfill(5)

    def __str__(self):
        return f'{self.type} - {self.amount}'


class Transfer(models.Model):
    driving_school = models.ForeignKey(
        'utilities.DrivingSchool',
        on_delete=models.CASCADE, related_name='transfers')
    creator = models.ForeignKey(
        'user_auth.User', on_delete=models.SET_NULL,
        null=True, blank=True)
    from_account = models.ForeignKey(
        BankAccount, on_delete=models.SET_NULL,
        null=True, blank=True, related_name='from_account')
    to_account = models.ForeignKey(
        BankAccount, on_delete=models.SET_NULL,
        null=True, blank=True, related_name='to_account')
    amount = models.FloatField(default=0)
    date = models.DateField()
    description = models.TextField(null=True, blank=True, default='')

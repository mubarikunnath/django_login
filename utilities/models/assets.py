from django.db import models


class Fleet(models.Model):
    driving_school = models.ForeignKey(
        'utilities.DrivingSchool',
        on_delete=models.CASCADE, related_name='fleets')
    name = models.CharField(max_length=64)
    purchase_date = models.DateField()
    purchase_amount = models.FloatField()
    file = models.URLField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    image = models.URLField(null=True, blank=True)

    def __str__(self):
        return self.name


class FleetDocument(models.Model):
    title = models.CharField(max_length=64)
    document = models.URLField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    fleet = models.ForeignKey(
        Fleet, on_delete=models.CASCADE, related_name='documents')

    def __str__(self):
        return self.title

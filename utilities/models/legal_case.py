from django.db import models

CASE_STATUS = (
    (1, 'Pending'),
    (2, 'Hearing'),
    (3, 'Closed')
)


class Lawyer(models.Model):
    driving_school = models.ForeignKey(
        'utilities.DrivingSchool',
        on_delete=models.CASCADE, related_name='lawyers')
    name = models.CharField(max_length=100)
    mobile = models.CharField(max_length=10)
    email = models.EmailField(null=True, blank=True)
    address = models.TextField(null=True, blank=True)
    court = models.CharField(max_length=64, null=True, blank=True)
    specialization = models.CharField(max_length=64, null=True, blank=True)

    def __str__(self):
        return self.user.name


class CaseType(models.Model):
    driving_school = models.ForeignKey(
        'utilities.DrivingSchool',
        on_delete=models.CASCADE, related_name='case_types')
    title = models.CharField(max_length=64, unique=True)

    def __str__(self):
        return self.title


class LegalCase(models.Model):
    driving_school = models.ForeignKey(
        'utilities.DrivingSchool',
        on_delete=models.CASCADE, related_name='legal_cases')
    lawyer = models.ForeignKey(Lawyer, on_delete=models.SET_NULL,
                               null=True, blank=True)
    title = models.CharField(max_length=64)
    description = models.TextField(null=True, blank=True)
    status = models.PositiveSmallIntegerField(choices=CASE_STATUS, default=1)
    case_type = models.ForeignKey(CaseType, on_delete=models.SET_NULL,
                                  null=True, blank=True)
    defendant = models.CharField(max_length=64, null=True, blank=True)
    judge = models.CharField(max_length=64, null=True, blank=True)
    court = models.CharField(max_length=64, null=True, blank=True)
    start_date = models.DateField(null=True, blank=True)
    next_hearing_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    plaintiff = models.CharField(max_length=64, null=True, blank=True)

    def __str__(self) -> str:
        return self.title

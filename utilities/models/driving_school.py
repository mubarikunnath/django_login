from django.db import models


class DrivingSchool(models.Model):
    owner = models.ForeignKey('user_auth.User', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    phone = models.CharField(max_length=12)
    cash_in_hand = models.FloatField(default=0)
    reflect_amount_on_bank_accounts = models.BooleanField(default=True)
    country = models.ForeignKey(
        'utilities.Country',
        null=True, blank=True,
        on_delete=models.PROTECT,
        related_name='driving_schools')
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class DrivingSchoolUser(models.Model):
    user = models.ForeignKey(
        'user_auth.User', on_delete=models.CASCADE,
        related_name='linked_driving_schools')
    driving_school = models.ForeignKey(
        'utilities.DrivingSchool',
        on_delete=models.PROTECT,
        related_name='linked_users')
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.user.name} - {self.driving_school.name}'

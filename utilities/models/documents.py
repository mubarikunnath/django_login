from django.db import models


class Document(models.Model):
    driving_school = models.ForeignKey(
        'utilities.DrivingSchool',
        on_delete=models.CASCADE, related_name='documents')
    name = models.CharField(max_length=64, blank=True)
    file = models.URLField()
    uploaded_at = models.DateTimeField(auto_now_add=True)
    description = models.TextField(null=True, blank=True)

    def __str__(self) -> str:
        return self.name

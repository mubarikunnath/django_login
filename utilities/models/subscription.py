from django.db import models

UNITS = (
    ('day', 'Day'),
    ('week', 'Week'),
    ('month', 'Month'),
    ('year', 'Year'),
    ('custom', 'Custom'),
)
STATUS = (
    ('pending', 'Pending'),
    ('success', 'Success'),
    ('failed', 'Failed'),
)


class SubscriptionType(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    country = models.ForeignKey('utilities.Country', on_delete=models.CASCADE)


class SubscriptionPlan(models.Model):
    subscription_type = models.ForeignKey(
        SubscriptionType, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    amount = models.FloatField()
    duration = models.IntegerField()
    duration_unit = models.CharField(max_length=16, choices=UNITS)
    days = models.IntegerField()
    is_active = models.BooleanField(default=True)
    is_free_trial = models.BooleanField(default=False)
    country = models.ForeignKey('utilities.Country', on_delete=models.CASCADE)


class SubscriptionTransaction(models.Model):
    amount = models.FloatField()
    status = models.CharField(
        max_length=16, choices=STATUS, default='pending')
    payment_id = models.CharField(
        max_length=100, null=True, blank=True)
    payment_date = models.DateTimeField(auto_now_add=True)
    payment_type = models.CharField(max_length=100, choices=(
        ('online', 'Online'), ('offline', 'Offline')))
    driving_school = models.ForeignKey(
        'utilities.DrivingSchool', on_delete=models.SET_NULL,
        null=True, blank=True)
    creator = models.ForeignKey(
        'user_auth.User', on_delete=models.SET_NULL,
        null=True, blank=True)


class Subscription(models.Model):
    driving_school = models.ForeignKey(
        'utilities.DrivingSchool', on_delete=models.CASCADE,
        related_name='subscriptions')
    subscription_plan = models.ForeignKey(
        SubscriptionPlan, on_delete=models.CASCADE)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    is_active = models.BooleanField(default=True)
    transaction = models.ForeignKey(
        SubscriptionTransaction,
        on_delete=models.SET_NULL, null=True, blank=True)
    is_paid = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)
    is_extended = models.BooleanField(default=False)
    is_trial = models.BooleanField(default=False)

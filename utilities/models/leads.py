from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

LEAD_STATUS = (
    (1, 'Pending'),
    (2, 'Following'),
    (3, 'Completed'),
)


def validate_mobile(value):
    if not (value.isnumeric() and len(value) >= 10):
        raise ValidationError(
            _(f'{value} is not a valid mobile number'))


class Lead(models.Model):
    driving_school = models.ForeignKey(
        'utilities.DrivingSchool',
        on_delete=models.CASCADE, related_name='leads')
    lead_by = models.CharField(max_length=64)
    contact = models.CharField(
        max_length=12, null=True, blank=True, validators=[validate_mobile])
    email = models.EmailField(null=True, blank=True)
    lead_value = models.CharField(max_length=32)
    status = models.PositiveSmallIntegerField(choices=LEAD_STATUS)

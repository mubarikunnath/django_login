from django.db import models


class Country(models.Model):
    name = models.CharField(max_length=32)
    code = models.CharField(max_length=5)
    currency = models.CharField(max_length=32)
    timezone = models.CharField(max_length=32)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'countries'
        ordering = ('name',)

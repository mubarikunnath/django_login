from rest_framework import serializers
from utilities.models import Fleet, FleetDocument


class FleetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fleet
        exclude = ('driving_school',)

    def create(self, validated_data):
        validated_data['driving_school'] = self.context.get(
            'user').driving_school
        return super().create(validated_data)


class FleetDocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = FleetDocument
        fields = '__all__'

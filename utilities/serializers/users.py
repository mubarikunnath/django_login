from rest_framework import serializers
from user_auth.models import User
from utilities.models import Transaction, ChequeDetails
from utilities.serializers import ChequeDetailsSerializer
from user_auth.models import StaffAttendance
from datetime import timedelta


class UserSerializer(serializers.ModelSerializer):
    _password = serializers.CharField(write_only=True, required=False)

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'mobile', 'name',
                  'designation', 'user_type', 'is_login', '_password')

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        if validated_data.get('email') and self.instance:
            if User.objects.filter(
                    email=validated_data['email'].lower()).exclude(
                        id=self.instance.id).exists():
                raise serializers.ValidationError(
                    {'email': 'Email already exists.'})
        elif validated_data.get('email'):
            if User.objects.filter(
                    email=validated_data['email'].lower()).exists():
                raise serializers.ValidationError(
                    {'email': 'Email already exists.'})
        return validated_data

    def create(self, validated_data):
        validated_data['driving_school'] = self.context.get(
            'user').driving_school
        password = None
        if validated_data.get('is_login') is True:
            if validated_data.get('_password') is None:
                raise serializers.ValidationError(
                    {'password': 'Password is required.'})
            else:
                password = validated_data.pop('_password')
        validated_data['username'] = validated_data['username'].lower()
        if validated_data.get('email'):
            validated_data['email'] = validated_data['email'].lower()
        user = super().create(validated_data)
        if password:
            user.set_password(password)
            user.save()
        return user

    def update(self, instance, validated_data):
        password = None
        if validated_data.get('_password'):
            password = validated_data.pop('_password')
        validated_data['username'] = validated_data['username'].lower()
        if validated_data.get('email'):
            validated_data['email'] = validated_data['email'].lower()
        user = super().update(instance, validated_data)
        if password:
            user.set_password(password)
            user.save()
        return user


class SalarySerializer(serializers.ModelSerializer):
    cheque_details = ChequeDetailsSerializer(
        required=False, write_only=True)

    class Meta:
        model = Transaction
        fields = ('id', 'amount', 'date', 'description', 'sub_category',
                  'mode_of_payment', 'bank_account', 'cheque_details')

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        if validated_data.get('mode_of_payment') in [3, 2]:
            if validated_data.get('bank_account') is None:
                raise serializers.ValidationError(
                    'bank details required. ')
        if validated_data.get('mode_of_payment') == 2:
            if validated_data.get('cheque_details') is None:
                raise serializers.ValidationError(
                    'cheque details required. ')
        return validated_data

    def create(self, validated_data):
        if validated_data.get('mode_of_payment') == 2:
            if not validated_data.get('cheque_details') is None:
                cheque_details = validated_data.pop('cheque_details')
                cheque_details = ChequeDetails.objects.create(**cheque_details)
                validated_data['cheque_details'] = cheque_details
        validated_data['creator'] = self.context['user']
        validated_data['is_salary'] = True
        validated_data['staff'] = self.context['staff']
        validated_data['type'] = 2
        validated_data['driving_school'] = self.context.get(
            'user').driving_school
        return super().create(validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['category'] = instance.sub_category.category.id
        return data


class StaffAttendanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = StaffAttendance
        fields = ('id',  'date', 'leave_type', 'reason')

    def create(self, validated_data):
        validated_data['user'] = self.context['user']
        return super().create(validated_data)


class StaffAttendanceCreateSerializer(serializers.ModelSerializer):
    start_date = serializers.DateField(write_only=True)
    end_date = serializers.DateField(write_only=True)

    class Meta:
        model = StaffAttendance
        fields = ('id',  'leave_type', 'reason', 'start_date', 'end_date')

    def create(self, validated_data):
        validated_data['user'] = self.context['user']
        start_date = validated_data.pop('start_date')
        end_date = validated_data.pop('end_date')
        if start_date == end_date:
            validated_data['date'] = start_date
            return super().create(validated_data)
        for _date in range((end_date - start_date).days + 1):
            validated_data['date'] = start_date + timedelta(days=_date)
            instance = super().create(validated_data)
        return instance

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['staff'] = instance.user.id
        return data

from rest_framework import serializers
from utilities.models import Document


class DocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        exclude = ('driving_school',)

    def create(self, validated_data):
        validated_data['driving_school'] = self.context.get(
            'user').driving_school
        return super().create(validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['display_uploaded_at'] = instance.uploaded_at.strftime('%d-%m-%Y')
        return data

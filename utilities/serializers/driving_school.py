from rest_framework import serializers
from utilities.models import (
    DrivingSchool,
    SubscriptionPlan,
    Subscription,
    DrivingSchoolUser

)
from utilities.serializers import get_cash_in_hand
from django.utils import timezone
from user_auth.models import User


class DrivingSchoolSerializer(serializers.ModelSerializer):
    class Meta:
        model = DrivingSchool
        fields = '__all__'

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['hand_cash_balance'] = get_cash_in_hand(instance)
        return data


class UserRegistrationSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'mobile', 'name',
                  'designation', 'password')

    def create(self, validated_data):
        password = validated_data.pop('password')
        validated_data['user_type'] = 2
        validated_data['username'] = validated_data['username'].lower()
        validated_data['email'] = validated_data['email'].lower()
        user = super().create(validated_data)
        user.set_password(password)
        user.save()
        return user


class RegisterDrivingSchoolSerializer(serializers.ModelSerializer):
    user = UserRegistrationSerializer(required=True, write_only=True)

    class Meta:
        model = DrivingSchool
        exclude = ('owner', 'reflect_amount_on_bank_accounts', 'cash_in_hand')

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        if User.objects.filter(
                username=validated_data['user']['username'].lower()).exists():
            raise serializers.ValidationError(
                {'username': 'Username already exists.'})
        if User.objects.filter(
                email=validated_data['user']['email'].lower()).exists():
            raise serializers.ValidationError(
                {'email': 'Email already exists.'})
        if User.objects.filter(
                mobile=validated_data['user']['mobile']).exists():
            raise serializers.ValidationError(
                {'mobile': 'Mobile number already exists.'})

        return validated_data

    def create(self, validated_data):
        user_data = validated_data.pop('user')
        user_serializer = UserRegistrationSerializer(data=user_data)
        user_serializer.is_valid(raise_exception=True)
        user = user_serializer.save()
        validated_data['owner'] = user
        plan = SubscriptionPlan.objects.get(
            is_free_trial=True, country=validated_data['country'])
        driving_school = DrivingSchool.objects.create(**validated_data)
        Subscription.objects.create(
            driving_school=driving_school,
            subscription_plan=plan,
            start_date=timezone.now(),
            end_date=timezone.now() + timezone.timedelta(days=plan.days),
            is_active=True,
            is_trial=True,
            is_paid=True
        )
        user.driving_school = driving_school
        user.save()
        DrivingSchoolUser.objects.create(
            driving_school=driving_school,
            user=user,
        )
        return driving_school

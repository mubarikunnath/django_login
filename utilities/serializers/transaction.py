from rest_framework import serializers
from utilities.models import BankAccount, ChequeDetails, TransactionCategory, \
    Transaction,  TransactionSubCategory, Transfer, DrivingSchool
from django.db import transaction as atomic_transaction
from django.db.models import Sum


def get_bank_balance(bank_account):
    if DrivingSchool.objects.exists():
        if DrivingSchool.objects.first().reflect_amount_on_bank_accounts:
            initial_balance = bank_account.account_balance
            incomes = sum(list(Transaction.objects.filter(
                type=1, bank_account=bank_account).values_list('amount', flat=True)))
            expenses = sum(list(Transaction.objects.filter(
                type=2, bank_account=bank_account).values_list('amount', flat=True)))
            transfer_in = sum(list(Transfer.objects.filter(
                to_account=bank_account).values_list('amount', flat=True)))
            transfer_out = sum(list(Transfer.objects.filter(
                from_account=bank_account).values_list('amount', flat=True)))
            return initial_balance + incomes - expenses + transfer_in - transfer_out
    return None


def get_cash_in_hand(obj):
    incomes = sum(list(Transaction.objects.filter(
        driving_school=obj,
        type=1, bank_account__isnull=True).values_list('amount', flat=True)))
    expenses = sum(list(Transaction.objects.filter(
        driving_school=obj,
        type=2, bank_account__isnull=True).values_list('amount', flat=True)))
    transfer_in = sum(list(Transfer.objects.filter(
        driving_school=obj,
        to_account__isnull=True,
        from_account__isnull=False).values_list('amount', flat=True)))
    transfer_out = sum(list(Transfer.objects.filter(
        driving_school=obj,
        to_account__isnull=False,
        from_account__isnull=True).values_list('amount', flat=True)))
    amount = incomes - expenses + transfer_in - transfer_out
    if obj:
        amount += obj.cash_in_hand
    return amount


class BankAccountSerializer(serializers.ModelSerializer):
    amount = serializers.SerializerMethodField()

    class Meta:
        model = BankAccount
        exclude = ('driving_school',)

    def create(self, validated_data):
        validated_data['driving_school'] = self.context.get(
            'user').driving_school
        return super().create(validated_data)

    def get_amount(self, obj):
        return get_bank_balance(obj)


class ChequeDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChequeDetails
        exclude = ('driving_school',)

    def create(self, validated_data):
        validated_data['driving_school'] = self.context.get(
            'user').driving_school
        return super().create(validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['display_cheque_date'] = instance.cheque_date.strftime('%d-%m-%Y')
        return data


class TransactionCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = TransactionCategory
        exclude = ('driving_school',)

    def create(self, validated_data):
        validated_data['driving_school'] = self.context.get(
            'user').driving_school
        return super().create(validated_data)


class TransactionSubCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = TransactionSubCategory
        exclude = ('category',)

    def create(self, validated_data):
        validated_data['category'] = self.context.get('category')
        return super().create(validated_data)


class TransactionSerializer(serializers.ModelSerializer):
    cheque_details = ChequeDetailsSerializer(required=False)
    receipt_id = serializers.CharField(read_only=True)

    class Meta:
        model = Transaction
        exclude = ('driving_school',)

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        if validated_data.get('category'):
            if validated_data.get('category').type != validated_data.get('type'):
                raise serializers.ValidationError(
                    'category type does not match')
        if validated_data['mode_of_payment'] == 3:
            if validated_data['bank_account'] is None:
                raise serializers.ValidationError(
                    {'bank_account': 'This field is required.'})
        elif validated_data['mode_of_payment'] == 2:
            if validated_data['cheque_details'] is None:
                raise serializers.ValidationError(
                    {'cheque_details': 'This field is required.'})
        if validated_data.get('is_student_transaction'):
            if validated_data.get('student') is None:
                raise serializers.ValidationError(
                    {'student': 'This field is required.'})
        return validated_data

    def create(self, validated_data):
        validated_data['creator'] = self.context.get('user')
        validated_data['driving_school'] = self.context.get(
            'user').driving_school
        if validated_data.get('cheque_details'):
            cheque_details_data = validated_data.pop('cheque_details')
            cheque_details = ChequeDetails.objects.create(
                **cheque_details_data)
            validated_data['cheque_details'] = cheque_details

        return super().create(validated_data)

    def update(self, instance, validated_data):
        with atomic_transaction.atomic():
            instance.delete()
            validated_data['creator'] = self.context.get('user')
            validated_data['driving_school'] = self.context.get(
                'user').driving_school
            if validated_data.get('cheque_details'):
                cheque_details_data = validated_data.pop('cheque_details')
                cheque_details = ChequeDetails.objects.create(
                    **cheque_details_data)
                validated_data['cheque_details'] = cheque_details

            instance = Transaction.objects.create(**validated_data)
        return instance

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.category:
            data['category_name'] = instance.category.title

        if instance.sub_category:
            data['sub_category_name'] = instance.sub_category.title
        data['display_date'] = instance.date.strftime('%d-%m-%Y')
        if instance.salary_date:
            data['display_salary_date'] = instance.salary_date.strftime(
                '%d-%m-%Y')
        if instance.creator:
            data['creator_name'] = instance.creator.name
        if instance.student:
            data['student_name'] = instance.student.name
            data['student_address'] = instance.student.address
            data['student_license'] = instance.student.license_number
            data['total_fees'] = instance.student.total_fees
            amount = Transaction.objects.filter(
                student=instance.student, type=1).aggregate(
                    total=Sum('amount'))['total']
            amount = amount if amount else 0
            data['balance'] = instance.student.total_fees - amount
        return data


class TransferSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transfer
        exclude = ('creator','driving_school',)

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        if validated_data.get('from_account') == validated_data.get('to_account'):
            raise serializers.ValidationError(
                'from and to account cannot be same')
        if validated_data.get('to_account') is None and \
                validated_data.get('from_account') is None:
            raise serializers.ValidationError(
                {'to_account': 'This field is required.'})

        return validated_data

    def create(self, validated_data):
        validated_data['creator'] = self.context.get('user')
        validated_data['driving_school'] = self.context.get(
            'user').driving_school
        return super().create(validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['display_date'] = instance.date.strftime('%d-%m-%Y')
        if instance.creator:
            data['creator_name'] = instance.creator.name
        return data

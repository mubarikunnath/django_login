from rest_framework import serializers
from utilities.models import Lawyer, LegalCase, CaseType


class LawyerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lawyer
        exclude = ('driving_school',)

    def create(self, validated_data):
        validated_data['driving_school'] = self.context.get(
            'user').driving_school
        return super().create(validated_data)


class CaseTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CaseType
        fields = ('id', 'title')

    def create(self, validated_data):
        validated_data['driving_school'] = self.context.get(
            'user').driving_school
        return super().create(validated_data)


class LegalCaseDetailSerializer(serializers.ModelSerializer):
    lawyer_name = serializers.SerializerMethodField()

    class Meta:
        model = LegalCase
        fields = '__all__'

    def get_lawyer_name(self, obj):
        return obj.lawyer.name if obj.lawyer else None

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.start_date:
            data['display_start_date'] = instance.start_date.strftime(
                '%d-%m-%Y')
        if instance.next_hearing_date:
            data['display_next_hearing_date'] = instance.next_hearing_date.strftime(
                '%d-%m-%Y')
        if instance.end_date:
            data['display_end_date'] = instance.end_date.strftime('%d-%m-%Y')
        return data


class LegalCaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = LegalCase
        exclude = ('driving_school',)
    
    def create(self, validated_data):
        validated_data['driving_school'] = self.context.get(
            'user').driving_school
        return super().create(validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.start_date:
            data['display_start_date'] = instance.start_date.strftime(
                '%d-%m-%Y')
        if instance.next_hearing_date:
            data['display_next_hearing_date'] = instance.next_hearing_date.strftime(
                '%d-%m-%Y')
        if instance.end_date:
            data['display_end_date'] = instance.end_date.strftime('%d-%m-%Y')
        return data


class LegalCaseListSerializer(serializers.ModelSerializer):
    status = serializers.CharField(source='get_status_display')

    class Meta:
        model = LegalCase
        fields = ('id',  'title', 'description', 'status',
                  'start_date')

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.lawyer:
            data['lawyer'] = instance.lawyer.name
        if instance.case_type:
            data['case_type'] = instance.case_type.title
        if instance.start_date:
            data['display_start_date'] = instance.start_date.strftime(
                '%d-%m-%Y')
        return data


class CaseCardSerializer(serializers.Serializer):
    no_of_lawyers = serializers.SerializerMethodField()
    pending_cases = serializers.SerializerMethodField()
    hearing_cases = serializers.SerializerMethodField()
    closed_cases = serializers.SerializerMethodField()

    def get_no_of_lawyers(self, obj):
        return Lawyer.objects.filter(
            driving_school=self.context.get('user').driving_school).count()

    def get_pending_cases(self, obj):
        return LegalCase.objects.filter(
            status=1,
            driving_school=self.context.get('user').driving_school).count()

    def get_hearing_cases(self, obj):
        return LegalCase.objects.filter(
            status=2,
            driving_school=self.context.get('user').driving_school).count()

    def get_closed_cases(self, obj):
        return LegalCase.objects.filter(
            status=3,
            driving_school=self.context.get('user').driving_school).count()

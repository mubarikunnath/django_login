from rest_framework import serializers
from utilities.models import Transaction, ChequeDetails, TransactionCategory
from utilities.serializers import ChequeDetailsSerializer
from user_auth.models import User, StaffAttendance
from datetime import date, timedelta
import calendar
from django.db import transaction as atomic_transaction


class SalariesListSerializer(serializers.ModelSerializer):
    staff_name = serializers.CharField(source='staff.name', read_only=True)
    mode_of_payment = serializers.CharField(
        source='get_mode_of_payment_display', read_only=True)

    class Meta:
        model = Transaction
        fields = ('id',  'amount', 'date', 'staff_name',
                  'salary_date', 'mode_of_payment',)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['display_date'] = instance.date.strftime('%d-%m-%Y')
        if instance.salary_date:
            data['display_salary_date'] = instance.salary_date.strftime(
                '%d-%m-%Y')
        return data


class SalariesCreateSerializer(serializers.ModelSerializer):
    cheque_details = ChequeDetailsSerializer(
        required=False, write_only=True)

    class Meta:
        model = Transaction
        fields = ('id', 'amount', 'date', 'description', 'salary_date',
                  'mode_of_payment', 'bank_account', 'cheque_details', 'staff')

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        if validated_data.get('mode_of_payment') in [3, 2]:
            if validated_data.get('bank_account') is None:
                raise serializers.ValidationError(
                    'bank details required. ')
        if validated_data.get('mode_of_payment') == 2:
            if validated_data.get('cheque_details') is None:
                raise serializers.ValidationError(
                    'cheque details required. ')
        return validated_data

    def create(self, validated_data):
        driving_school = self.context.get(
                'user').driving_school
        validated_data['driving_school'] = driving_school
        category, _ = TransactionCategory.objects.get_or_create(
                title='Salary', type=2, driving_school=driving_school)
        if validated_data.get('mode_of_payment') == 2:
            if not validated_data.get('cheque_details') is None:
                cheque_details = validated_data.pop('cheque_details')
                cheque_details = ChequeDetails.objects.create(**cheque_details)
                validated_data['cheque_details'] = cheque_details
        validated_data['category'] = category
        validated_data['creator'] = self.context['user']
        validated_data['is_salary'] = True
        validated_data['type'] = 2
        instance = Transaction.objects.create(**validated_data)
        return instance

    def update(self, instance, validated_data):
        with atomic_transaction.atomic():
            instance.delete()
            driving_school = self.context.get(
                'user').driving_school
            validated_data['driving_school'] = driving_school
            category, _ = TransactionCategory.objects.get_or_create(
                title='Salary', type=2, driving_school=driving_school)
            if validated_data.get('mode_of_payment') == 2:
                if not validated_data.get('cheque_details') is None:
                    cheque_details = validated_data.pop('cheque_details')
                    cheque_details = ChequeDetails.objects.create(
                        **cheque_details)
                    validated_data['cheque_details'] = cheque_details
            validated_data['category'] = category
            validated_data['creator'] = self.context['user']
            validated_data['is_salary'] = True
            validated_data['type'] = 2
            return super().create(validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['display_date'] = instance.date.strftime('%d-%m-%Y')
        if instance.salary_date:
            data['display_salary_date'] = instance.salary_date.strftime(
                '%d-%m-%Y')
        return data


class SalariesUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Transaction
        fields = ('id', 'amount', 'date', 'description', 'salary_date',
                  'bank_account', 'staff')

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['display_date'] = instance.date.strftime('%d-%m-%Y')
        if instance.salary_date:
            data['display_salary_date'] = instance.salary_date.strftime(
                '%d-%m-%Y')
        return data


class UsersAttendanceListSerializer(serializers.ModelSerializer):
    attendance = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'name', 'attendance')

    def get_attendance(self, obj):
        month = self.context['month']
        year = self.context['year']
        attendances = StaffAttendance.objects.filter(
            user=obj,
            date__month=month,
            date__year=year).order_by('-date')
        first_day = date(int(year), int(month), 1)
        number_of_days = calendar.monthrange(int(year), int(month))[1]
        data = []
        for i in range(number_of_days):
            day = first_day + timedelta(days=i)
            if attendances.filter(date=day).exists():
                attendance = attendances.filter(date=day).first()
                data.append(
                    {'id': attendance.id,
                     'date': attendance.date,
                     'status': attendance.get_leave_type_display(),
                     'reason': attendance.reason})
                continue
            if day <= date.today():
                data.append(
                    {'id': None,
                     'date': day,
                     'status': 'Present',
                     'reason': None})
            else:
                data.append(
                    {'id': None,
                     'date': day,
                     'status': None,
                     'reason': None})

        return data

from rest_framework import serializers
from utilities.models import Lead


class LeadsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lead
        exclude = ('driving_school',)

    def create(self, validated_data):
        validated_data['driving_school'] = self.context.get(
            'user').driving_school
        return super().create(validated_data)

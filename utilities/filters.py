from coreapi import Field
from rest_framework.filters import BaseFilterBackend
from datetime import datetime, timedelta, date
import calendar


class TransactionFilterBackend(BaseFilterBackend):
    def get_schema_fields(self, view):
        return [
            Field(
                name='type',
                location='query',
                required=False,
                type='string'
            ),
            Field(
                name='from_date',
                location='query',
                required=False,
                type='string'
            ),
            Field(
                name='to_date',
                location='query',
                required=False,
                type='string'
            ),
            Field(
                name='category',
                location='query',
                required=False,
                type='string'
            ),
            Field(
                name='sub_category',
                location='query',
                required=False,
                type='string'
            ),


        ]

    def filter_queryset(self, request, queryset, view):
        _type = request.query_params.get('type', 1)
        category = request.query_params.get('category', None)
        sub_category = request.query_params.get('sub_category', None)
        last_day = calendar.monthrange(
            date.today().year, date.today().month)[1]
        from_date = request.query_params.get(
            'from_date', date.today().replace(day=1))
        to_date = request.query_params.get(
            'to_date', date.today().replace(day=last_day))
        if type(to_date) == str:
            to_date = datetime.strptime(to_date, '%Y-%m-%d').date()
        if _type:
            queryset = queryset.filter(type=_type)
        if category:
            queryset = queryset.filter(category__id=category)
        if sub_category:
            queryset = queryset.filter(sub_category__id=sub_category)
        return queryset.filter(date__range=[
            from_date, to_date+timedelta(days=1)])


class TransactionCategoryFilterBackend(BaseFilterBackend):
    def get_schema_fields(self, view):
        return [
            Field(
                name='type',
                location='query',
                required=False,
                type='string'
            ),

        ]

    def filter_queryset(self, request, queryset, view):
        type = request.query_params.get('type', 1)

        if type:
            queryset = queryset.filter(type=type)
        return queryset


class TransactionSubCategoryFilterBackend(BaseFilterBackend):
    def get_schema_fields(self, view):
        return [
            Field(
                name='type',
                location='query',
                required=False,
                type='string'
            ),

        ]

    def filter_queryset(self, request, queryset, view):
        type = request.query_params.get('type', 1)

        if type:
            queryset = queryset.filter(category__type=type)
        return queryset


class MonthYearFilterBackend(BaseFilterBackend):
    def get_schema_fields(self, view):
        return [
            Field(
                name='month',
                location='query',
                required=False,
                type='string'
            ),
            Field(
                name='year',
                location='query',
                required=False,
                type='string'
            ),
        ]

    def filter_queryset(self, request, queryset, view):
        month = request.query_params.get('month', date.today().month)
        year = request.query_params.get('year', date.today().year)

        queryset = queryset.filter(
            salary_date__month=month,
            salary_date__year=year
        )
        return queryset


class UserFilterBackend(BaseFilterBackend):
    def get_schema_fields(self, view):
        return [
            Field(
                name='is_deleted_user',
                location='query',
                required=False,
                type='string'
            ),
        ]

    def filter_queryset(self, request, queryset, view):
        is_deleted_user = request.query_params.get('is_deleted_user', False)
        if is_deleted_user:
            queryset = queryset.filter(is_deleted_user=True)
        else:
            queryset = queryset.filter(is_deleted_user=False)
        return queryset

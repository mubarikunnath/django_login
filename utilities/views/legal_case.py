from user_auth.permission import CustomListCreateAPIView, \
    CustomRetrieveUpdateDestroyAPIView
from rest_framework.generics import GenericAPIView
from utilities.serializers import LawyerSerializer, \
    CaseTypeSerializer, CaseCardSerializer, LegalCaseSerializer, \
    LegalCaseListSerializer, LegalCaseDetailSerializer
from user_auth.permission import IsAuthenticatedUser
from utilities.models import CaseType, LegalCase, Lawyer
from drivezy.response import SuccessResponse
from rest_framework import filters


class LawyerListCreateView(CustomListCreateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = LawyerSerializer
    queryset = Lawyer.objects.all().order_by('-id')
    filter_backends = [filters.SearchFilter, ]
    search_fields = ['name', 'email', 'phone', 'address']

    def get_queryset(self):
        return super().get_queryset().filter(
            driving_school=self.request.user.driving_school)

    def get_serializer_context(self):
        return {'user': self.request.user}


class LawyerDetailView(CustomRetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser,)
    queryset = Lawyer.objects.all()
    serializer_class = LawyerSerializer
    allowed_methods = ['GET', 'PATCH', 'DELETE']


class CaseTypeView(CustomListCreateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = CaseTypeSerializer
    queryset = CaseType.objects.all().order_by('-id')

    def get_queryset(self):
        return super().get_queryset().filter(
            driving_school=self.request.user.driving_school)

    def get_serializer_context(self):
        return {'user': self.request.user}


class CaseTypeDetailView(CustomRetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = CaseTypeSerializer
    queryset = CaseType.objects.all()
    allowed_methods = ['PATCH', 'DELETE']


class LegalCaseView(CustomListCreateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    queryset = LegalCase.objects.all().order_by('-id')
    filter_backends = [filters.SearchFilter, ]
    search_fields = ['lawyer__name', 'title',
                     'description', 'case_type__title']

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return LegalCaseListSerializer
        return LegalCaseSerializer

    def get_queryset(self):
        return super().get_queryset().filter(
            driving_school=self.request.user.driving_school)

    def get_serializer_context(self):
        return {'user': self.request.user}


class LegalCaseDetailView(CustomRetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = LegalCaseDetailSerializer
    queryset = LegalCase.objects.all()
    allowed_methods = ['GET', 'PATCH', 'DELETE']


class CaseCardView(GenericAPIView):
    permission_classes = (IsAuthenticatedUser,)

    def get(self, request):
        serializer = CaseCardSerializer({}, context={'user': request.user})
        return SuccessResponse(data=serializer.data, message="Success")

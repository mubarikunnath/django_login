from user_auth.permission import CustomListCreateAPIView, \
    CustomRetrieveUpdateDestroyAPIView
from user_auth.permission import IsAuthenticatedUser
from utilities.models import Lead
from utilities.serializers import LeadsSerializer
from rest_framework import filters


class LeadsListCreateView(CustomListCreateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = LeadsSerializer
    queryset = Lead.objects.all()
    filter_backends = [filters.SearchFilter, ]
    search_fields = ['lead_by', 'contact']

    def get_queryset(self):
        return super().get_queryset().filter(
            driving_school=self.request.user.driving_school)

    def get_serializer_context(self):
        return {'user': self.request.user}


class LeadUpdateView(CustomRetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = LeadsSerializer
    queryset = Lead.objects.all()
    allowed_methods = ['GET', 'PATCH', 'DELETE']

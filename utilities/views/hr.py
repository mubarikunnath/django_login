from user_auth.permission import CustomListCreateAPIView, \
    CustomRetrieveUpdateDestroyAPIView, CustomListAPIView
from rest_framework.generics import GenericAPIView
from user_auth.permission import IsAuthenticatedUser
from utilities.models import Transaction
from utilities.serializers import SalariesListSerializer,\
    SalariesCreateSerializer, UsersAttendanceListSerializer, \
    SalariesUpdateSerializer
from user_auth.models import User
from student.models import Student
from datetime import date
from drivezy.response import SuccessResponse
from utilities.filters import MonthYearFilterBackend
from django.db.models import Q


class SalariesListCreateAPIView(CustomListCreateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    queryset = Transaction.objects.filter(is_salary=True).order_by('-id')
    filter_backends = (MonthYearFilterBackend,)

    def get_queryset(self):
        return super().get_queryset().filter(
            driving_school=self.request.user.driving_school)

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return SalariesListSerializer
        return SalariesCreateSerializer

    def get_serializer_context(self):
        return {
            'user': self.request.user,
        }


class SalariesRetrieveUpdateDestroyAPIView(CustomRetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser,)
    queryset = Transaction.objects.filter(is_salary=True)
    allowed_methods = ('GET', 'PATCH', 'DELETE')

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return SalariesCreateSerializer
        return SalariesUpdateSerializer

    def get_serializer_context(self):
        return {
            'user': self.request.user,
        }


class UsersAttendanceListView(CustomListAPIView):
    permission_classes = (IsAuthenticatedUser,)
    queryset = User.objects.all().order_by('-id')
    serializer_class = UsersAttendanceListSerializer
    # filter_backends = (MonthYearFilterBackendTwo,)

    def get_queryset(self):
        self.queryset = super().get_queryset().filter(
            driving_school=self.request.user.driving_school)
        month = self.request.query_params.get('month', date.today().month)
        year = self.request.query_params.get('year', date.today().year)
        return self.queryset.filter(Q(is_deleted_user=False) | Q(
            is_deleted_user=True, deleted_date__month=month,
            deleted_date__year=year))

    def get_serializer_context(self):
        month = self.request.query_params.get('month', date.today().month)
        year = self.request.query_params.get('year', date.today().year)
        return {
            'month': month,
            'year': year,
        }


class DashboardCardDataView(GenericAPIView):
    permission_classes = (IsAuthenticatedUser,)

    def get(self, request, *args, **kwargs):
        staffs_count = User.objects.filter(
            is_deleted_user=False,
            driving_school=request.user.driving_school).count()
        total_student = Student.objects.filter(
            is_verified=True,
            driving_school=request.user.driving_school).count()
        req_students = Student.objects.filter(
            is_verified=False,
            driving_school=request.user.driving_school).count()
        return SuccessResponse(message='Success', data={
            'staffs_count': staffs_count,
            'total_student': total_student,
            'req_students': req_students
        })


class DashboardFinancialReportView(GenericAPIView):
    permission_classes = (IsAuthenticatedUser,)
    # filter_backends = (AttendanceFilterBackend,)

    def get(self, request, *args, **kwargs):
        year = self.request.query_params.get('year', date.today().year)
        month = self.request.query_params.get('month', date.today().month)
        income = sum(list(Transaction.objects.filter(
            driving_school=request.user.driving_school,
            type=1, date__year=year,
            date__month=month).values_list('amount', flat=True)))
        expense = sum(list(Transaction.objects.filter(
            driving_school=request.user.driving_school,
            type=2, date__year=year,
            date__month=month).values_list('amount', flat=True)))
        last_transaction = Transaction.objects.filter(
            driving_school=request.user.driving_school).order_by('-id').first()
        if not last_transaction:
            last_transaction_amount = 0
        else:
            last_transaction_amount = last_transaction.amount
        if not last_transaction:
            last_transaction_date = None
        else:
            last_transaction_date = last_transaction.date.strftime("%d-%m-%Y")
        return SuccessResponse(message='Success', data={
            'income': income,
            'expense': expense,
            'last_transaction_amount': last_transaction_amount,
            'last_transaction_date': last_transaction_date,

        })

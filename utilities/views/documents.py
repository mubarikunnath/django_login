from user_auth.permission import CustomListCreateAPIView, \
    CustomRetrieveUpdateDestroyAPIView
from user_auth.permission import IsAuthenticatedUser
from utilities.serializers import DocumentSerializer
from utilities.models import Document
from rest_framework import filters


class DocumentView(CustomListCreateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = DocumentSerializer
    queryset = Document.objects.all().order_by('-id')
    filter_backends = [filters.SearchFilter, ]
    search_fields = ['name', 'description']

    def get_queryset(self):
        return super().get_queryset().filter(
            driving_school=self.request.user.driving_school)

    def get_serializer_context(self):
        return {'user': self.request.user}


class DocumentDetailView(CustomRetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = DocumentSerializer
    queryset = Document.objects.all()
    allowed_methods = ['GET', 'PATCH', 'DELETE']

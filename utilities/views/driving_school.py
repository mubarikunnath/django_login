from django.shortcuts import get_object_or_404
from user_auth.permission import (
    CustomRetrieveUpdateAPIView,

)
from rest_framework.generics import (
    GenericAPIView,
    CreateAPIView
)
from utilities.models import (
    DrivingSchool,

)
from user_auth.permission import (
    IsAuthenticatedUser,
    IsOwnerOrAdmin,
    CustomListAPIView
)
from utilities.serializers import (
    DrivingSchoolSerializer,
    RegisterDrivingSchoolSerializer
)
from drivezy.response import (
    SuccessResponse
)


class DrivingSchoolView(CustomRetrieveUpdateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = DrivingSchoolSerializer
    allowed_methods = ('GET', 'PATCH')

    def get_object(self):
        return self.request.user.driving_school


class DrivingSchoolListView(CustomListAPIView):
    permission_classes = (IsAuthenticatedUser, IsOwnerOrAdmin)
    serializer_class = DrivingSchoolSerializer

    def get_queryset(self):
        return DrivingSchool.objects.filter(
            linked_users__user=self.request.user)


class SwitchDrivingSchool(GenericAPIView):
    permission_classes = (IsAuthenticatedUser, IsOwnerOrAdmin)

    def patch(self, request, *args, **kwargs):
        id = kwargs.get('pk')
        driving_school = get_object_or_404(DrivingSchool, id=id)
        request.user.driving_school = driving_school
        request.user.save()
        return SuccessResponse(message='Driving school switched successfully')


class RegisterDrivingSchoolView(CreateAPIView):
    serializer_class = RegisterDrivingSchoolSerializer

from rest_framework.generics import (
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView
)
from utilities.models import Fleet, FleetDocument
from utilities.serializers import (
    FleetSerializer,
    FleetDocumentSerializer
)
from user_auth.permission import IsAuthenticatedUser
from rest_framework import filters
from drivezy.response import SuccessResponse
from user_auth.permission import (
    CustomListCreateAPIView,
    CustomRetrieveUpdateDestroyAPIView

)


class FleetListCreateView(CustomListCreateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = FleetSerializer
    queryset = Fleet.objects.all()
    filter_backends = (filters.SearchFilter, )
    search_fields = ('name', 'description')

    def get_queryset(self):
        return super().get_queryset().filter(
            driving_school=self.request.user.driving_school)

    def get_serializer_context(self):
        return {'user': self.request.user}


class FleetDetailView(CustomRetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = FleetSerializer
    queryset = Fleet.objects.all()
    allowed_methods = ['GET', 'PATCH', 'DELETE']

    def get_queryset(self):
        return super().get_queryset().filter(
            driving_school=self.request.user.driving_school)

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Success')


class FleetDocumentListCreateView(CustomListCreateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = FleetDocumentSerializer

    def get_queryset(self):
        return FleetDocument.objects.filter(
            fleet__id=self.kwargs.get('fleet_id'))


class FleetDocumentDetailView(CustomRetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = FleetDocumentSerializer
    allowed_methods = ['GET', 'PATCH', 'DELETE']

    def get_queryset(self):
        return FleetDocument.objects.filter(
            fleet__id=self.kwargs.get('fleet_id'))

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Success')

from user_auth.permission import CustomListCreateAPIView, \
    CustomRetrieveUpdateDestroyAPIView
from user_auth.permission import IsAuthenticatedUser
from user_auth.models import User
from utilities.serializers import UserSerializer, SalarySerializer, \
    StaffAttendanceSerializer, StaffAttendanceCreateSerializer
from django.shortcuts import get_object_or_404
from user_auth.models import StaffAttendance
from drivezy.response import SuccessResponse
from django.utils import timezone
from utilities.filters import UserFilterBackend
from rest_framework import filters


class UsersListCreateView(CustomListCreateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    queryset = User.objects.filter(is_deleted_user=False).order_by('-id')
    serializer_class = UserSerializer
    filter_backends = (UserFilterBackend, filters.SearchFilter, )
    search_fields = ['name', 'username', 'mobile', 'designation']

    def get_queryset(self):
        return super().get_queryset().filter(
            driving_school=self.request.user.driving_school)

    def get_serializer_context(self):
        return {'user': self.request.user}


class UserDetailedView(CustomRetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser,)
    queryset = User.objects.all()
    serializer_class = UserSerializer
    allowed_methods = ('GET', 'PATCH', 'DELETE')

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.is_deleted_user = True
        instance.deleted_date = timezone.now()
        instance.save()
        return SuccessResponse(message='Success fully deleted user')


class StaffSalariesListCreateView(CustomListCreateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = SalarySerializer

    def get_queryset(self):
        user = get_object_or_404(User, pk=self.kwargs.get('pk'))
        return user.salaries.select_related().filter(
            is_salary=True).order_by('-date')

    def get_serializer_context(self):
        user = get_object_or_404(User, pk=self.kwargs.get('pk'))
        return {'user': self.request.user, 'staff': user}


class StaffAttendanceListCreateView(CustomListCreateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = StaffAttendanceSerializer
    # filter_backends = (AttendanceFilterBackend,)

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return StaffAttendanceSerializer
        return StaffAttendanceCreateSerializer

    def get_queryset(self):
        user = get_object_or_404(User, pk=self.kwargs.get('pk'))
        return user.attendances.all().order_by('-date')

    def get_serializer_context(self):
        user = get_object_or_404(User, pk=self.kwargs.get('pk'))
        return {'user': user}


class StaffAttendanceDetailedView(CustomRetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = StaffAttendanceSerializer
    queryset = StaffAttendance.objects.all()
    allowed_methods = ('GET', 'PATCH', 'DELETE')

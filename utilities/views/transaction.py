from user_auth.permission import CustomListCreateAPIView, \
    CustomRetrieveUpdateDestroyAPIView, \
    CustomRetrieveUpdateAPIView
from rest_framework.generics import GenericAPIView
from user_auth.permission import IsAuthenticatedUser
from utilities.models import BankAccount, ChequeDetails, TransactionCategory, \
    Transaction,  Transfer, TransactionSubCategory
from utilities.serializers import BankAccountSerializer, \
    ChequeDetailsSerializer, TransactionCategorySerializer, \
    TransactionSerializer,  \
    TransactionSubCategorySerializer, TransferSerializer
from drivezy.response import SuccessResponse
from django.shortcuts import get_object_or_404
from utilities.filters import TransactionFilterBackend, \
    TransactionCategoryFilterBackend, TransactionSubCategoryFilterBackend
from datetime import date, datetime
import calendar
from dateutil import relativedelta


class BankAccountListCreateView(CustomListCreateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = BankAccountSerializer

    def get_queryset(self):
        return BankAccount.objects.filter(
            driving_school=self.request.user.driving_school).order_by('-id')

    def get_serializer_context(self):
        return {'user': self.request.user}


class BankAccountDetailedView(CustomRetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser,)
    queryset = BankAccount.objects.all()
    serializer_class = BankAccountSerializer
    allowed_methods = ('GET', 'PATCH', 'DELETE')

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Success')


class ChequeDetailsListCreateView(CustomListCreateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = ChequeDetailsSerializer

    def get_queryset(self):
        return ChequeDetails.objects.filter(
            driving_school=self.request.user.driving_school).order_by('-id')

    def get_serializer_context(self):
        return {'user': self.request.user}


class ChequeDetailsDetailedView(CustomRetrieveUpdateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    queryset = ChequeDetails.objects.all()
    serializer_class = ChequeDetailsSerializer
    allowed_methods = ('GET', 'PATCH', 'DELETE')


class TransactionCategoryListCreateView(CustomListCreateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = TransactionCategorySerializer
    filter_backends = (TransactionCategoryFilterBackend,)

    def get_queryset(self):
        return TransactionCategory.objects.filter(
            driving_school=self.request.user.driving_school).order_by('-id')

    def get_serializer_context(self):
        return {'user': self.request.user}


class TransactionCategoryDetailedView(CustomRetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser,)
    queryset = TransactionCategory.objects.all()
    serializer_class = TransactionCategorySerializer
    allowed_methods = ('GET', 'PATCH', 'DELETE')

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Success')


class TransactionSubCategoryListCreateView(CustomListCreateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = TransactionSubCategorySerializer
    filter_backends = (TransactionSubCategoryFilterBackend,)

    def get_queryset(self):
        category = get_object_or_404(
            TransactionCategory, pk=self.kwargs['pk'])
        return category.sub_categories.all().order_by('-id')

    def get_serializer_context(self):
        category = get_object_or_404(TransactionCategory, pk=self.kwargs['pk'])
        return {'category': category}


class TransactionSubCategoryDetailedView(CustomRetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser,)
    queryset = TransactionSubCategory.objects.all()
    serializer_class = TransactionSubCategorySerializer
    allowed_methods = ('GET', 'PATCH', 'DELETE')

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Success')


class TransactionListCreateView(CustomListCreateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    queryset = Transaction.objects.select_related(
        'creator',).all().order_by('-id')
    serializer_class = TransactionSerializer
    filter_backends = (TransactionFilterBackend,)

    def get_queryset(self):
        return super().get_queryset().filter(
            driving_school=self.request.user.driving_school)

    def get_serializer_context(self):
        return {'user': self.request.user}


class TransactionDetailedView(CustomRetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser,)
    queryset = Transaction.objects.select_related(
        'creator',).all()
    serializer_class = TransactionSerializer
    allowed_methods = ('GET', 'PATCH', 'DELETE')

    def get_serializer_context(self):
        return {'user': self.request.user}

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Success')


class TransferListCreateView(CustomListCreateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    queryset = Transfer.objects.all().order_by('-date')
    serializer_class = TransferSerializer

    def get_queryset(self):
        return super().get_queryset().filter(
            driving_school=self.request.user.driving_school)

    def get_serializer_context(self):
        return {'user': self.request.user}


class TransferDetailedView(CustomRetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser,)
    queryset = Transfer.objects.all()
    serializer_class = TransferSerializer
    allowed_methods = ('GET', 'PATCH', 'DELETE')

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Success')


class FinancialReportView(GenericAPIView):
    permission_classes = (IsAuthenticatedUser,)

    def get(self, request, *args, **kwargs):
        data = []
        total_categorized_income = 0
        total_categorized_expense = 0
        income_category_data = []
        expense_category_data = []
        from_date = date.today().replace(day=1)
        last_day = calendar.monthrange(from_date.year, from_date.month)[1]
        to_date = date.today().replace(day=last_day)

        from_date = request.query_params.get('from_date', from_date)
        to_date = request.query_params.get('to_date', to_date)

        if type(from_date) == str:
            from_date = datetime.strptime(from_date, '%Y-%m-%d').date()
        if type(to_date) == str:
            to_date = datetime.strptime(to_date, '%Y-%m-%d').date()
        income = sum(list(Transaction.objects.filter(
            driving_school=self.request.user.driving_school,
            date__range=[from_date, to_date],
            type=1).values_list('amount', flat=True)))
        expense = sum(list(Transaction.objects.filter(
            driving_school=self.request.user.driving_school,
            date__range=[from_date, to_date],
            type=2).values_list('amount', flat=True)))
        if income == 0:
            income = 1
        if expense == 0:
            expense = 1
        months = (to_date.year - from_date.year) * \
            12 + to_date.month - from_date.month
        if months == 0:
            months = 1
        for month in range(months):
            _date = from_date + relativedelta.relativedelta(months=month)
            year = _date.year
            month = _date.month
            income_month = sum(list(Transaction.objects.filter(
                driving_school=self.request.user.driving_school,
                date__year=year, date__month=month,
                type=1).values_list('amount', flat=True)))
            expense_month = sum(list(Transaction.objects.filter(
                driving_school=self.request.user.driving_school,
                date__year=year, date__month=month,
                type=2).values_list('amount', flat=True)))

            data.append({'year': year, 'month': _date.strftime("%b"),
                        'income': income_month, 'expense': expense_month})

        income_categories = TransactionCategory.objects.filter(
            type=1, driving_school=self.request.user.driving_school)
        expense_categories = TransactionCategory.objects.filter(
            type=2, driving_school=self.request.user.driving_school)
        for income_category in income_categories:
            amount = sum(list(Transaction.objects.filter(
                driving_school=self.request.user.driving_school,
                date__range=[from_date, to_date],
                category=income_category).values_list('amount', flat=True)))
            if amount == 0:
                continue
            percentage = (amount / income) * 100
            income_category_data.append(
                {'name': income_category.title,
                 'amount': amount,
                 'percentage': percentage})
            total_categorized_income += amount
        for expense_category in expense_categories:
            amount = sum(list(Transaction.objects.filter(
                driving_school=self.request.user.driving_school,
                date__range=[from_date, to_date],
                category=expense_category).values_list('amount', flat=True)))
            if amount == 0:
                continue
            percentage = (amount / expense) * 100
            expense_category_data.append(
                {'name': expense_category.title,
                 'amount': amount,
                 'percentage': percentage})
            total_categorized_expense += amount
        income_category_data.append(
            {
                'name': 'Non Categorized',
                'amount': income - total_categorized_income,
                'percentage': ((income - total_categorized_income) / income) * 100
            }
        )
        expense_category_data.append(
            {
                'name': 'Non Categorized',
                'amount': expense - total_categorized_expense,
                'percentage': ((expense - total_categorized_expense) / expense) * 100
            }
        )
        return SuccessResponse(
            message='Success',
            data={
                'income': income,
                'expense': expense,
                'data': data,
                'income_category_data': income_category_data,
                'expense_category_data': expense_category_data
            })

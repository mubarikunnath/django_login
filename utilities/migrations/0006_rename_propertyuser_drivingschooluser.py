# Generated by Django 4.0.6 on 2022-10-26 05:53

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('utilities', '0005_rename_price_subscriptionplan_amount_propertyuser'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='PropertyUser',
            new_name='DrivingSchoolUser',
        ),
    ]

# Generated by Django 4.0.6 on 2022-10-25 06:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('utilities', '0002_remove_transactionsubcategory_driving_school'),
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=32)),
                ('code', models.CharField(max_length=5)),
                ('currency', models.CharField(max_length=32)),
                ('timezone', models.CharField(max_length=32)),
            ],
            options={
                'verbose_name_plural': 'countries',
                'ordering': ('name',),
            },
        ),
        migrations.AddField(
            model_name='drivingschool',
            name='country',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='driving_schools', to='utilities.country'),
        ),
    ]

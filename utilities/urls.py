from django.urls import path
from utilities.views import (
    LawyerListCreateView,
    LawyerDetailView,
    CaseTypeView,
    CaseTypeDetailView,
    CaseCardView,
    LegalCaseView,
    LegalCaseDetailView,
    DocumentView,
    DocumentDetailView,
    TransactionDetailedView,
    TransactionCategoryDetailedView,
    TransactionCategoryListCreateView,
    TransactionListCreateView,
    ChequeDetailsDetailedView,
    ChequeDetailsListCreateView,
    BankAccountDetailedView,
    BankAccountListCreateView,
    UserDetailedView,
    UsersListCreateView,
    TransactionSubCategoryDetailedView,
    TransactionSubCategoryListCreateView,
    StaffSalariesListCreateView,
    StaffAttendanceDetailedView,
    StaffAttendanceListCreateView,
    SalariesListCreateAPIView,
    SalariesRetrieveUpdateDestroyAPIView,
    UsersAttendanceListView,
    DashboardCardDataView,
    DashboardFinancialReportView,
    DrivingSchoolView,
    TransferListCreateView,
    TransferDetailedView,
    FinancialReportView,
    FleetListCreateView,
    FleetDetailView,
    FleetDocumentListCreateView,
    FleetDocumentDetailView,
    LeadsListCreateView,
    LeadUpdateView,
    DrivingSchoolListView,
    SwitchDrivingSchool,
    RegisterDrivingSchoolView


)
urlpatterns = [

    # legal_case
    path('lawyers/', LawyerListCreateView.as_view()),
    path('lawyer/<int:pk>/', LawyerDetailView.as_view()),
    path('case_types/', CaseTypeView.as_view()),
    path('case_type/<int:pk>/', CaseTypeDetailView.as_view()),
    path('legal_cases/', LegalCaseView.as_view()),
    path('legal_case/<int:pk>/', LegalCaseDetailView.as_view()),
    path('case_card/', CaseCardView.as_view()),



    # documents
    path('documents/', DocumentView.as_view()),
    path('document/<int:pk>/', DocumentDetailView.as_view()),

    # bank account
    path('bank_accounts/', BankAccountListCreateView.as_view()),
    path('bank_account/<int:pk>/', BankAccountDetailedView.as_view()),



    # transaction
    path('transactions/', TransactionListCreateView.as_view()),
    path('transaction/<int:pk>/', TransactionDetailedView.as_view()),

    # transaction category
    path('transaction_categories/',
         TransactionCategoryListCreateView.as_view()),
    path('transaction_category/<int:pk>/',
         TransactionCategoryDetailedView.as_view()),
    path('transaction_category/<int:pk>/sub_categories/',
         TransactionSubCategoryListCreateView.as_view()),
    path('transaction_sub_category/<int:pk>/',
         TransactionSubCategoryDetailedView.as_view()),

    # check details
    path('cheque_details/', ChequeDetailsListCreateView.as_view()),
    path('cheque_detail/<int:pk>/', ChequeDetailsDetailedView.as_view()),

    # user
    path('users/', UsersListCreateView.as_view()),
    path('user/<int:pk>/', UserDetailedView.as_view()),


    path('user/<int:pk>/staff_salaries/',
         StaffSalariesListCreateView.as_view()),

    path('user/<int:pk>/staff_attendance/',
         StaffAttendanceListCreateView.as_view()),
    path('staff_attendance/<int:pk>/',
         StaffAttendanceDetailedView.as_view()),

    # hr
    path('hr/salaries/', SalariesListCreateAPIView.as_view()),
    path('hr/salary/<int:pk>/',
         SalariesRetrieveUpdateDestroyAPIView.as_view()),
    path('hr/attendance/', UsersAttendanceListView.as_view()),

    # dashboard
    path('dashboard/', DashboardCardDataView.as_view()),
    path('dashboard/financial_report/',
         DashboardFinancialReportView.as_view()),

    # business
    path('driving_school/', DrivingSchoolView.as_view()),


    # transfer
    path('transfer/', TransferListCreateView.as_view()),
    path('transfer/<int:pk>/', TransferDetailedView.as_view()),


    # financial report
    path('financial_report/', FinancialReportView.as_view()),



    # fleet
    path('fleets/', FleetListCreateView.as_view()),
    path('fleet/<int:pk>/', FleetDetailView.as_view()),
    path('fleet/<int:fleet_id>/fleet_documents/',
         FleetDocumentListCreateView.as_view()),
    path('fleet/<int:fleet_id>/fleet_document/<int:pk>/',
         FleetDocumentDetailView.as_view()),

    # leads
    path('leads/', LeadsListCreateView.as_view()),
    path('lead/<int:pk>/', LeadUpdateView.as_view()),

    # driving schools list (only for admins and owners)
    path('driving_schools/', DrivingSchoolListView.as_view()),
    path('switch_driving_school/<int:pk>/',
         SwitchDrivingSchool.as_view()),

    # register driving school
    path('register_driving_school/', RegisterDrivingSchoolView.as_view()),

]

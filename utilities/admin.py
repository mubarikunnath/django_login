from django.contrib import admin

# Register your models here.
from utilities.models import (
    Lawyer,
    LegalCase,
    CaseType,
    Document,
    Transaction,
    TransactionCategory,
    BankAccount,
    ChequeDetails,
    TransactionSubCategory,
    DrivingSchool,
    Transfer,
    Fleet,
    Country,
    SubscriptionType,
    SubscriptionPlan,
    SubscriptionTransaction,
    Subscription,
    DrivingSchoolUser
)


class LawyerAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'email', 'mobile', 'address')
    search_fields = ('name', 'email', 'mobile', 'address')


class CaseTypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'title',)
    search_fields = ('title',)


class LegalCaseAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'status', 'case_type',
                    'start_date', 'end_date')
    search_fields = ('title', 'status', 'case_type', 'start_date', 'end_date')


class VisitorLogAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'phone', 'created_user',
                    'purpose', 'visitor_id')
    search_fields = ('name', 'phone', 'created_user',
                     'purpose', 'visitor_id')


class DocumentAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'uploaded_at', 'description')
    search_fields = ('name',  'uploaded_at', 'description')


class TransactionAdmin(admin.ModelAdmin):
    list_display = ('id', 'creator',  'amount', 'date',
                    'description', 'sub_category', 'type')
    search_fields = ('creator',  'amount', 'date',
                     'description', 'sub_category', 'type')


class TransactionCategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'title',)
    search_fields = ('title',)


class BankAccountAdmin(admin.ModelAdmin):
    list_display = ('id', 'account_number', 'account_name', 'account_balance',
                    'upi', 'ifsc', 'bank_name', 'bank_branch', 'bank_address')
    search_fields = ('account_number', 'account_name', 'account_balance',
                     'upi', 'ifsc', 'bank_name', 'bank_branch', 'bank_address')


class CheckDetailsAdmin(admin.ModelAdmin):
    list_display = ('id', 'cheque_number', 'cheque_date', 'cheque_amount',
                    'cheque_status', 'cheque_remarks')
    search_fields = ('cheque_number', 'cheque_date', 'cheque_amount',
                     'cheque_status', 'cheque_remarks')


class TransactionSubCategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'category')
    search_fields = ('title', 'category')


class DrivingSchoolAdmin(admin.ModelAdmin):
    list_display = ('name', 'phone', 'address')
    search_fields = ('name', 'phone', 'address', )


class TransferAdmin(admin.ModelAdmin):
    list_display = ('id', 'from_account', 'to_account', 'amount', 'date')
    search_fields = ('from_account', 'to_account', 'amount', 'date')


class FleetAdmin(admin.ModelAdmin):
    list_display = ('id', 'driving_school', 'name', 'purchase_date')
    search_fields = ('driving_school', 'name',  'purchase_date')


class CountryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'code', 'currency', 'timezone')
    search_fields = ('name', 'code', 'currency', 'timezone')


class SubscriptionTypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'description')
    search_fields = ('name', 'description')


class SubscriptionPlanAdmin(admin.ModelAdmin):
    list_display = ('id', 'subscription_type',
                    'amount', 'duration', 'description')
    search_fields = ('subscription_type', 'amount', 'duration', 'description')


class SubscriptionTransactionAdmin(admin.ModelAdmin):
    list_display = ('id', 'subscription', 'amount', 'payment_date')
    search_fields = ('subscription', 'amount', 'date')


class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ('id', 'driving_school',
                    'subscription_plan', 'start_date', 'end_date')
    search_fields = ('driving_school',
                     'subscription_plan', 'start_date', 'end_date')


class DrivingSchoolUserAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'driving_school')
    search_fields = ('user', 'driving_school')


admin.site.register(TransactionSubCategory, TransactionSubCategoryAdmin)
admin.site.register(TransactionCategory, TransactionCategoryAdmin)
admin.site.register(BankAccount, BankAccountAdmin)
admin.site.register(ChequeDetails, CheckDetailsAdmin)
admin.site.register(Document, DocumentAdmin)
admin.site.register(Lawyer, LawyerAdmin)
admin.site.register(CaseType, CaseTypeAdmin)
admin.site.register(LegalCase, LegalCaseAdmin)
admin.site.register(Transaction, TransactionAdmin)
admin.site.register(DrivingSchool, DrivingSchoolAdmin)
admin.site.register(Transfer, TransferAdmin)
admin.site.register(Fleet, FleetAdmin)
admin.site.register(Country, CountryAdmin)
admin.site.register(SubscriptionType, SubscriptionTypeAdmin)
admin.site.register(SubscriptionPlan, SubscriptionPlanAdmin)
admin.site.register(SubscriptionTransaction, SubscriptionTransactionAdmin)
admin.site.register(Subscription, SubscriptionAdmin)
admin.site.register(DrivingSchoolUser, DrivingSchoolUserAdmin)

from django.urls import path
from mobile.views import (
    StudentListCreateView

)
urlpatterns = [
    path('students/', StudentListCreateView.as_view()),
]

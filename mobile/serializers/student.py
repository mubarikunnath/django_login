from rest_framework import serializers
from student.models import Student


class MobileStudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        exclude = ('user', 'application_number', 'rto_class_date',
                   'is_verified', 'license_number', 'total_fees',
                   'passed_date', 'is_rejected')

    def create(self, validated_data):
        validated_data['user'] = self.context.get('user')
        return super().create(validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['class_of_vehicle'] = instance.class_of_vehicle.values(
            'id', 'vehicle_type')
        return data


class MobileStudentUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        exclude = ('user', 'application_number', 'rto_class_date',
                   'is_verified', 'license_number', 'total_fees',
                   'passed_date', 'is_rejected')
        
    def validate(self, attrs):
        return super().validate(attrs)

from rest_framework.generics import (
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView
)
from student.models import Student
from mobile.serializers import (
    MobileStudentSerializer,
    MobileStudentUpdateSerializer
)
from drivezy.response import SuccessResponse, ErrorResponse


class StudentListCreateView(ListCreateAPIView):
    queryset = Student.objects.all()
    serializer_class = MobileStudentSerializer

    def get_queryset(self):
        return super().get_queryset().filter(
            user=self.request.user)

    def get_serializer_context(self):
        return {'user': self.request.user}


class StudentUpdateDestroyView(RetrieveUpdateDestroyAPIView):
    queryset = Student.objects.all()
    serializer_class = MobileStudentUpdateSerializer

    def get_queryset(self):
        return super().get_queryset().filter(
            user=self.request.user)

    def get_serializer_context(self):
        return {'user': self.request.user}

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.is_verified:
            message = "You can't delete a verified student"
            return ErrorResponse(message=message)
        instance.delete()
        return SuccessResponse(message="Student deleted successfully")

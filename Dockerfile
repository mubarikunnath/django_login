FROM python:3

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1


WORKDIR /srv/drivezy/app
COPY ./requirements.txt  /srv/drivezy/app
RUN pip install -r requirements.txt

# copy project
COPY . /srv/drivezy/app
from rest_framework import serializers
from utilities.models import (
    DrivingSchool,
    DrivingSchoolUser
)
from user_auth.models import User


class AdminPanelUserListSerializer(serializers.ModelSerializer):
    user_type = serializers.CharField(source='get_user_type_display')

    class Meta:
        model = User
        fields = ('id', 'name', 'username', 'mobile', 'email',
                  'profile_photo', 'user_type', )

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.driving_school:
            data['driving_school'] = instance.driving_school.name
        return data


class AdminPanelUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'name', 'username', 'mobile', 'email',
                  'profile_photo', 'user_type', 'password',
                  'mobile_verified', 'email_verified', 'is_login',
                  'basic_registration', 'driving_school')
        extra_kwargs = {'password': {'write_only': True, 'required': False}}

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user

    def update(self, instance, validated_data):
        password = validated_data.pop('password', None)
        instance = super(AdminPanelUserSerializer, self).update(
            instance, validated_data)
        if password:
            instance.set_password(password)
            instance.save()
        return instance

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.driving_school:
            data['driving_school'] = instance.driving_school.name
        return data

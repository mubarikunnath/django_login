from django.shortcuts import get_object_or_404
from rest_framework import serializers
from utilities.models import (
    DrivingSchool,
    DrivingSchoolUser,
    FleetDocument,
    Fleet,
    Document,
    Lead
)
from student.models import (
    Vehicle,
    RTO

)


class AdminDrivingSchoolSerializer(serializers.ModelSerializer):
    class Meta:
        model = DrivingSchool
        fields = '__all__'

    def create(self, validated_data):
        user = validated_data.get('user')
        driving_school = super().create(validated_data)
        DrivingSchoolUser.objects.create(
            driving_school=driving_school,
            user=user
        )
        user.driving_school = driving_school
        user.save()
        return driving_school

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['owner_name'] = instance.owner.name
        return data


class AdminDrivingSchoolVehiclesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vehicle
        fields = '__all__'

    def create(self, validated_data):
        driving_school = get_object_or_404(
            DrivingSchool, id=self.context.get('driving_school_id'))
        validated_data['driving_school'] = driving_school
        return super().create(validated_data)


class AdminPanelFleetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fleet
        exclude = ('driving_school',)

    def create(self, validated_data):
        driving_school = get_object_or_404(
            DrivingSchool, id=self.context.get('driving_school_id'))
        validated_data['driving_school'] = driving_school
        return super().create(validated_data)


class AdminPanelFleetDocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = FleetDocument
        exclude = ('fleet',)

    def create(self, validated_data):
        fleet = get_object_or_404(
            Fleet, id=self.context.get('fleet_id'))
        validated_data['fleet'] = fleet
        return super().create(validated_data)


class AdminPanelDocumentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
        exclude = ('driving_school',)

    def create(self, validated_data):
        driving_school = get_object_or_404(
            DrivingSchool, id=self.context.get('driving_school_id'))
        validated_data['driving_school'] = driving_school
        return super().create(validated_data)


class AdminPanelLeadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lead
        exclude = ('driving_school',)

    def create(self, validated_data):
        driving_school = get_object_or_404(
            DrivingSchool, id=self.context.get('driving_school_id'))
        validated_data['driving_school'] = driving_school
        return super().create(validated_data)


class AdminPanelRTOSerializer(serializers.ModelSerializer):
    class Meta:
        model = RTO
        exclude = ('driving_school',)

    def create(self, validated_data):
        driving_school = get_object_or_404(
            DrivingSchool, id=self.context.get('driving_school_id'))
        validated_data['driving_school'] = driving_school
        return super().create(validated_data)

    
from django.shortcuts import get_object_or_404
from rest_framework import serializers

from utilities.models import (
    LegalCase,
    Lawyer,
    CaseType,
    DrivingSchool

)


class AdminPanelLawyerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lawyer
        exclude = ('driving_school',)

    def create(self, validated_data):
        driving_school = get_object_or_404(
            DrivingSchool, pk=self.context.get('driving_school_id'))
        validated_data['driving_school'] = driving_school
        return super().create(validated_data)


class AdminPanelCaseTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CaseType
        exclude = ('driving_school',)

    def create(self, validated_data):
        driving_school = get_object_or_404(
            DrivingSchool, pk=self.context.get('driving_school_id'))
        validated_data['driving_school'] = driving_school
        return super().create(validated_data)


class AdminPanelLegalCaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = LegalCase
        exclude = ('driving_school',)

    def create(self, validated_data):
        driving_school = get_object_or_404(
            DrivingSchool, pk=self.context.get('driving_school_id'))
        validated_data['driving_school'] = driving_school
        return super().create(validated_data)

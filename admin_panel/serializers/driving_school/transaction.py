from django.shortcuts import get_object_or_404
from rest_framework import serializers
from utilities.models import (
    BankAccount,
    DrivingSchool,
    ChequeDetails,
    TransactionCategory,
    TransactionSubCategory,
    Transfer,
    Transaction

)
from utilities.serializers import ChequeDetailsSerializer
from django.db import transaction as atomic_transaction
from django.db.models import Sum


class AdminPanelBankAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = BankAccount
        exclude = ('driving_school',)

    def create(self, validated_data):
        driving_school = get_object_or_404(
            DrivingSchool, id=self.context.get('driving_school_id'))
        validated_data['driving_school'] = driving_school
        return super().create(validated_data)


class AdminPanelChequeDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ChequeDetails
        exclude = ('driving_school',)

    def create(self, validated_data):
        driving_school = get_object_or_404(
            DrivingSchool, id=self.context.get('driving_school_id'))
        validated_data['driving_school'] = driving_school
        return super().create(validated_data)


class AdminPanelTransactionCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = TransactionCategory
        exclude = ('driving_school',)

    def create(self, validated_data):
        driving_school = get_object_or_404(
            DrivingSchool, id=self.context.get('driving_school_id'))
        validated_data['driving_school'] = driving_school
        return super().create(validated_data)


class AdminPanelTransactionSubCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = TransactionSubCategory
        exclude = ('category',)

    def create(self, validated_data):
        category = get_object_or_404(
            TransactionCategory, id=self.context.get('category_id'))
        validated_data['category'] = category
        return super().create(validated_data)


class AdminPanelTransferSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transfer
        exclude = ('driving_school',)

    def create(self, validated_data):
        driving_school = get_object_or_404(
            DrivingSchool, id=self.context.get('driving_school_id'))
        validated_data['driving_school'] = driving_school
        return super().create(validated_data)


class AdminPanelTransactionSerializer(serializers.ModelSerializer):
    cheque_details = ChequeDetailsSerializer(required=False)
    receipt_id = serializers.CharField(read_only=True)

    class Meta:
        model = Transaction
        exclude = ('driving_school',)

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        if validated_data.get('category'):
            if validated_data.get('category').type != validated_data.get('type'):
                raise serializers.ValidationError(
                    'category type does not match')
        if validated_data['mode_of_payment'] == 3:
            if validated_data['bank_account'] is None:
                raise serializers.ValidationError(
                    {'bank_account': 'This field is required.'})
        elif validated_data['mode_of_payment'] == 2:
            if validated_data['cheque_details'] is None:
                raise serializers.ValidationError(
                    {'cheque_details': 'This field is required.'})
        if validated_data.get('is_student_transaction'):
            if validated_data.get('student') is None:
                raise serializers.ValidationError(
                    {'student': 'This field is required.'})
        return validated_data

    def create(self, validated_data):
        validated_data['creator'] = self.context.get('user')
        driving_school = get_object_or_404(
            DrivingSchool, id=self.context.get('driving_school_id'))
        validated_data['driving_school'] = driving_school
        if validated_data.get('cheque_details'):
            cheque_details_data = validated_data.pop('cheque_details')
            cheque_details = ChequeDetails.objects.create(
                **cheque_details_data)
            validated_data['cheque_details'] = cheque_details

        return super().create(validated_data)

    def update(self, instance, validated_data):
        with atomic_transaction.atomic():
            instance.delete()
            validated_data['creator'] = self.context.get('user')
            driving_school = get_object_or_404(
                DrivingSchool, id=self.context.get('driving_school_id'))
            validated_data['driving_school'] = driving_school
            if validated_data.get('cheque_details'):
                cheque_details_data = validated_data.pop('cheque_details')
                cheque_details = ChequeDetails.objects.create(
                    **cheque_details_data)
                validated_data['cheque_details'] = cheque_details

            instance = Transaction.objects.create(**validated_data)
        return instance

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if instance.category:
            data['category_name'] = instance.category.title

        if instance.sub_category:
            data['sub_category_name'] = instance.sub_category.title
        data['display_date'] = instance.date.strftime('%d-%m-%Y')
        if instance.salary_date:
            data['display_salary_date'] = instance.salary_date.strftime(
                '%d-%m-%Y')
        if instance.creator:
            data['creator_name'] = instance.creator.name
        if instance.student:
            data['student_name'] = instance.student.name
            data['student_address'] = instance.student.address
            data['student_license'] = instance.student.license_number
            data['total_fees'] = instance.student.total_fees
            amount = Transaction.objects.filter(
                student=instance.student, type=1).aggregate(
                    total=Sum('amount'))['total']
            amount = amount if amount else 0
            data['balance'] = instance.student.total_fees - amount
        return data

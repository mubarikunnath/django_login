
from django.shortcuts import get_object_or_404
from rest_framework import serializers
from utilities.models import (
    DrivingSchool,

)
from student.models import (
    Student,
    StudentFile,
    StudentClass,
    DrivingTest,
    LearningTest
)
from user_auth.models import (
    User
)


class DrivingSchoolStudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        exclude = ('driving_school', 'user')

    def create(self, validated_data):
        driving_school = get_object_or_404(
            DrivingSchool, id=self.context.get('driving_school_id'))
        validated_data['driving_school'] = driving_school
        if User.objects.filter(
                username=validated_data['mobile_number']).exists():
            user = User.objects.get(
                username=validated_data['mobile_number'])
        else:
            user = User.objects.create(
                mobile=validated_data['mobile_number'],
                username=validated_data['mobile_number'],
                name=validated_data['name'],
                user_type=5
            )
        validated_data['user'] = user
        return super().create(validated_data)

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['user'] = instance.user.name
        data['driving_school'] = instance.driving_school.name
        return data


class DrivingSchoolStudentFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentFile
        exclude = ('student',)

    def create(self, validated_data):
        student = get_object_or_404(
            Student, id=self.context.get('student_id'))
        validated_data['student'] = student
        return super().create(validated_data)


class AdminPanelStudentClassSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentClass
        exclude = ('student',)

    def create(self, validated_data):
        student = get_object_or_404(
            Student, id=self.context.get('student_id'))
        validated_data['student'] = student
        return super().create(validated_data)


class AdminPanelDrivingTestSerializer(serializers.ModelSerializer):
    class Meta:
        model = DrivingTest
        exclude = ('student',)

    def create(self, validated_data):
        student = get_object_or_404(
            Student, id=self.context.get('student_id'))
        validated_data['student'] = student
        return super().create(validated_data)


class AdminPanelLearningTestSerializer(serializers.ModelSerializer):
    class Meta:
        model = LearningTest
        exclude = ('student',)

    def create(self, validated_data):
        student = get_object_or_404(
            Student, id=self.context.get('student_id'))
        validated_data['student'] = student
        return super().create(validated_data)

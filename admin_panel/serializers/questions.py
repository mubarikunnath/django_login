from rest_framework import serializers
from student.models import Question


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = '__all__'

    def validate(self, attrs):
        validated_data = super().validate(attrs)
        if validated_data['answer_type'] == 'text' and not validated_data['answer']:
            raise serializers.ValidationError(
                'Answer is required for text type questions')
        if validated_data['answer_type'] == 'file' and not validated_data['answer_file']:
            raise serializers.ValidationError(
                'Answer file is required for file type questions')
        return validated_data

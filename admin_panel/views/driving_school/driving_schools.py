from rest_framework.generics import (
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView
)
from user_auth.permission import (
    IsAuthenticatedUser,
    IsAdmin
)
from utilities.models import (
    DrivingSchool,
    Fleet,
    FleetDocument,
    Document,
    Lead,

)
from student.models import (
    Vehicle,
    RTO

)
from admin_panel.serializers import (
    AdminDrivingSchoolSerializer,
    AdminDrivingSchoolVehiclesSerializer,
    AdminPanelFleetSerializer,
    AdminPanelFleetDocumentSerializer,
    AdminPanelDocumentSerializer,
    AdminPanelLeadSerializer,
    AdminPanelRTOSerializer


)
from drivezy.response import (
    SuccessResponse,
)


class DrivingSchoolListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    queryset = DrivingSchool.objects.all()
    serializer_class = AdminDrivingSchoolSerializer


class DrivingSchoolVehiclesListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminDrivingSchoolVehiclesSerializer

    def get_queryset(self):
        return Vehicle.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def get_serializer_context(self):
        return {'driving_school_id': self.kwargs.get('driving_school_id')}


class DrivingSchoolVehiclesUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminDrivingSchoolVehiclesSerializer
    allowed_methods = ['GET', 'PATCH', 'DELETE']

    def get_queryset(self):
        return Vehicle.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Vehicle deleted successfully')


class AdminPanelFleetListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelFleetSerializer

    def get_queryset(self):
        return Fleet.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def get_serializer_context(self):
        return {'driving_school_id': self.kwargs.get('driving_school_id')}


class AdminPanelFleetUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelFleetSerializer
    allowed_methods = ['GET', 'PATCH', 'DELETE']

    def get_queryset(self):
        return Fleet.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Fleet deleted successfully')


class AdminPanelFleetDocumentsListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelFleetDocumentSerializer

    def get_queryset(self):
        return FleetDocument.objects.filter(
            fleet__id=self.kwargs.get('fleet_id'))

    def get_serializer_context(self):
        return {'fleet_id': self.kwargs.get('fleet_id')}


class AdminPanelFleetDocumentsUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelFleetDocumentSerializer
    allowed_methods = ['GET', 'PATCH', 'DELETE']

    def get_queryset(self):
        return FleetDocument.objects.filter(
            fleet__id=self.kwargs.get('fleet_id'))

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Fleet document deleted successfully')


class AdminPanelDocumentsListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelDocumentSerializer

    def get_queryset(self):
        return Document.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def get_serializer_context(self):
        return {'driving_school_id': self.kwargs.get('driving_school_id')}


class AdminPanelDocumentsUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelDocumentSerializer
    allowed_methods = ['GET', 'PATCH', 'DELETE']

    def get_queryset(self):
        return Document.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Document deleted successfully')


class AdminPanelLeadsListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelLeadSerializer

    def get_queryset(self):
        return Lead.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def get_serializer_context(self):
        return {'driving_school_id': self.kwargs.get('driving_school_id')}


class AdminPanelLeadsUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelLeadSerializer
    allowed_methods = ['GET', 'PATCH', 'DELETE']

    def get_queryset(self):
        return Lead.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Lead deleted successfully')


class RTOListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelRTOSerializer

    def get_queryset(self):
        return RTO.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def get_serializer_context(self):
        return {'driving_school_id': self.kwargs.get('driving_school_id')}


class RTOUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelRTOSerializer
    allowed_methods = ['GET', 'PATCH', 'DELETE']

    def get_queryset(self):
        return RTO.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='RTO deleted successfully')

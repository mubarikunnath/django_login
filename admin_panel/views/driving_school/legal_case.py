from rest_framework.generics import (
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView
)
from user_auth.permission import (
    IsAuthenticatedUser,
    IsAdmin
)

from utilities.models import (
    LegalCase,
    Lawyer,
    CaseType,
    DrivingSchool

)

from admin_panel.serializers import (
    AdminPanelLawyerSerializer,
    AdminPanelCaseTypeSerializer,
    AdminPanelLegalCaseSerializer,
)
from drivezy.response import (
    SuccessResponse,

)


class AdminPanelLawyerListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelLawyerSerializer

    def get_queryset(self):
        return Lawyer.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def get_serializer_context(self):
        return {'driving_school_id': self.kwargs.get('driving_school_id')}


class AdminPanelLawyerUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelLawyerSerializer
    allowed_methods = ['GET', 'PATCH', 'DELETE']

    def get_queryset(self):
        return Lawyer.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Lawyer deleted successfully')


class AdminPanelCaseTypeListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelCaseTypeSerializer

    def get_queryset(self):
        return CaseType.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def get_serializer_context(self):
        return {'driving_school_id': self.kwargs.get('driving_school_id')}


class AdminPanelCaseTypeUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelCaseTypeSerializer
    allowed_methods = ['GET', 'PATCH', 'DELETE']

    def get_queryset(self):
        return CaseType.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='CaseType deleted successfully')


class AdminPanelLegalCaseListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelLegalCaseSerializer

    def get_queryset(self):
        return LegalCase.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def get_serializer_context(self):
        return {'driving_school_id': self.kwargs.get('driving_school_id')}


class AdminPanelLegalCaseUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelLegalCaseSerializer
    allowed_methods = ['GET', 'PATCH', 'DELETE']

    def get_queryset(self):
        return LegalCase.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='LegalCase deleted successfully')

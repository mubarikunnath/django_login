from rest_framework.generics import (
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView
)
from user_auth.permission import (
    IsAuthenticatedUser,
    IsAdmin
)

from utilities.models import (
    Transaction,
    TransactionCategory,
    TransactionSubCategory,
    BankAccount,
    ChequeDetails,
    Transfer


)
from admin_panel.serializers import (
    AdminPanelBankAccountSerializer,
    AdminPanelChequeDetailsSerializer,
    AdminPanelTransactionCategorySerializer,
    AdminPanelTransactionSubCategorySerializer,
    AdminPanelTransferSerializer,
    AdminPanelTransactionSerializer
)
from drivezy.response import (
    SuccessResponse,
)


class AdminPanelBankAccountListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelBankAccountSerializer

    def get_queryset(self):
        return BankAccount.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def get_serializer_context(self):
        return {'driving_school_id': self.kwargs.get('driving_school_id')}


class AdminPanelBankAccountUpdateDestroyView(
        RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelBankAccountSerializer
    allowed_methods = ['GET', 'PATCH', 'DELETE']

    def get_queryset(self):
        return BankAccount.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def get_serializer_context(self):
        return {
            'driving_school_id': self.kwargs.get('driving_school_id')
        }

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Bank Account deleted successfully')


class AdminPanelChequeDetailsListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelChequeDetailsSerializer

    def get_queryset(self):
        return ChequeDetails.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def get_serializer_context(self):
        return {'driving_school_id': self.kwargs.get('driving_school_id')}


class AdminPanelChequeDetailsUpdateDestroyView(
        RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelChequeDetailsSerializer
    allowed_methods = ['GET', 'PATCH', 'DELETE']

    def get_queryset(self):
        return ChequeDetails.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def get_serializer_context(self):
        return {
            'driving_school_id': self.kwargs.get('driving_school_id')
        }

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Cheque Details deleted successfully')


class AdminPanelTransactionCategoryListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelTransactionCategorySerializer

    def get_queryset(self):
        return TransactionCategory.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def get_serializer_context(self):
        return {
            'driving_school_id': self.kwargs.get('driving_school_id')}


class AdminPanelTransactionCategoryUpdateDestroyView(
        RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelTransactionCategorySerializer
    allowed_methods = ['GET', 'PATCH', 'DELETE']

    def get_queryset(self):
        return TransactionCategory.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def get_serializer_context(self):
        return {
            'driving_school_id': self.kwargs.get('driving_school_id')
        }

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(
            message='Transaction Category deleted successfully')


class AdminPanelTransactionSubCategoryListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelTransactionSubCategorySerializer

    def get_queryset(self):
        return TransactionSubCategory.objects.filter(
            category__id=self.kwargs.get('category_id'))

    def get_serializer_context(self):
        return {
            'category__id': self.kwargs.get('category_id')}


class AdminPanelTransactionSubCategoryUpdateDestroyView(
        RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelTransactionSubCategorySerializer
    allowed_methods = ['GET', 'PATCH', 'DELETE']

    def get_queryset(self):
        return TransactionSubCategory.objects.filter(
            category__id=self.kwargs.get('category_id'))

    def get_serializer_context(self):
        return {
            'category__id': self.kwargs.get('category_id')
        }

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(
            message='Transaction Sub Category deleted successfully')


class AdminPanelTransferListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelTransferSerializer

    def get_queryset(self):
        return Transfer.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def get_serializer_context(self):
        return {'driving_school_id': self.kwargs.get('driving_school_id')}


class AdminPanelTransferUpdateDestroyView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelTransferSerializer
    allowed_methods = ['GET', 'PATCH', 'DELETE']

    def get_queryset(self):
        return Transfer.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def get_serializer_context(self):
        return {
            'driving_school_id': self.kwargs.get('driving_school_id')
        }

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Transfer deleted successfully')


class AdminPanelTransactionListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelTransactionSerializer

    def get_queryset(self):
        return Transaction.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def get_serializer_context(self):
        return {
            'driving_school_id': self.kwargs.get('driving_school_id'),
            'user': self.request.user
        }


class AdminPanelTransactionUpdateDestroyView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelTransactionSerializer
    allowed_methods = ['GET', 'PATCH', 'DELETE']

    def get_queryset(self):
        return Transaction.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def get_serializer_context(self):
        return {
            'driving_school_id': self.kwargs.get('driving_school_id'),
            'user': self.request.user
        }

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Transaction deleted successfully')

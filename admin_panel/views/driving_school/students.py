from rest_framework.generics import (
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView
)
from user_auth.permission import (
    IsAuthenticatedUser,
    IsAdmin
)

from student.models import (
    Student,
    StudentFile,
    StudentClass,
    DrivingTest,
    LearningTest

)
from admin_panel.serializers import (
    DrivingSchoolStudentSerializer,
    DrivingSchoolStudentFileSerializer,
    AdminPanelDrivingTestSerializer,
    AdminPanelStudentClassSerializer,
    AdminPanelLearningTestSerializer,
)
from drivezy.response import (
    SuccessResponse,
)


class DrivingSchoolStudentsListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = DrivingSchoolStudentSerializer

    def get_queryset(self):
        return Student.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def get_serializer_context(self):
        return {'driving_school_id': self.kwargs.get('driving_school_id')}


class DrivingSchoolStudentsUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = DrivingSchoolStudentSerializer
    allowed_methods = ['GET', 'PATCH', 'DELETE']

    def get_queryset(self):
        return Student.objects.filter(
            driving_school__id=self.kwargs.get('driving_school_id'))

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Student deleted successfully')


class StudentFilesListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = DrivingSchoolStudentFileSerializer

    def get_queryset(self):
        return StudentFile.objects.filter(
            student__id=self.kwargs.get('student_id'))

    def get_serializer_context(self):
        return {'student_id': self.kwargs.get('student_id')}


class StudentFilesUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = DrivingSchoolStudentFileSerializer
    allowed_methods = ['GET', 'PATCH', 'DELETE']

    def get_queryset(self):
        return StudentFile.objects.filter(
            student__id=self.kwargs.get('student_id'))

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Student file deleted successfully')


class StudentClassesListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelStudentClassSerializer

    def get_queryset(self):
        return StudentClass.objects.filter(
            student__id=self.kwargs.get('student_id'))

    def get_serializer_context(self):
        return {'student_id': self.kwargs.get('student_id')}


class StudentClassesUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelStudentClassSerializer
    allowed_methods = ['GET', 'PATCH', 'DELETE']

    def get_queryset(self):
        return StudentClass.objects.filter(
            student__id=self.kwargs.get('student_id'))

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Student class deleted successfully')


class DrivingTestsListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelDrivingTestSerializer

    def get_queryset(self):
        return DrivingTest.objects.filter(
            student__id=self.kwargs.get('student_id'))

    def get_serializer_context(self):
        return {'student_id': self.kwargs.get('student_id')}


class DrivingTestsUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelDrivingTestSerializer
    allowed_methods = ['GET', 'PATCH', 'DELETE']

    def get_queryset(self):
        return DrivingTest.objects.filter(
            student__id=self.kwargs.get('student_id'))

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Driving test deleted successfully')


class LearningTestsListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelLearningTestSerializer

    def get_queryset(self):
        return LearningTest.objects.filter(
            student__id=self.kwargs.get('student_id'))

    def get_serializer_context(self):
        return {'student_id': self.kwargs.get('student_id')}


class LearningTestsUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    serializer_class = AdminPanelLearningTestSerializer
    allowed_methods = ['GET', 'PATCH', 'DELETE']

    def get_queryset(self):
        return LearningTest.objects.filter(
            student__id=self.kwargs.get('student_id'))

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Learning test deleted successfully')

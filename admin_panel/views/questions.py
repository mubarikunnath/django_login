from rest_framework.generics import (
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView,
)

from user_auth.permission import (
    IsAdmin,
    IsAuthenticatedUser
)

from admin_panel.serializers import QuestionSerializer
from student.models import Question
from drivezy.response import SuccessResponse


class QuestionsListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    queryset = Question.objects.all().order_by('-created_at')
    serializer_class = QuestionSerializer


class QuestionUpdateDeleteView(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer

    def delete(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.delete()
        return SuccessResponse(message='Question deleted successfully')

from rest_framework.generics import (
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView
)
from user_auth.permission import (
    IsAuthenticatedUser,
    IsAdmin
)
from utilities.models import (
    DrivingSchool,
)

from admin_panel.serializers import (
    AdminPanelUserSerializer,
    AdminPanelUserListSerializer,
)
from user_auth.models import User
from admin_panel.filters import (
    AdminPanelUserFilterBackend
)
from drivezy.response import (
    SuccessResponse,
    ErrorResponse
)


class AdminPanelUsersListCreateView(ListCreateAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    queryset = User.objects.all()
    filter_backends = (AdminPanelUserFilterBackend,)

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return AdminPanelUserListSerializer
        return AdminPanelUserSerializer


class AdminPanelUserUpdateDeleteVew(RetrieveUpdateDestroyAPIView):
    permission_classes = (IsAuthenticatedUser, IsAdmin)
    queryset = User.objects.all()
    serializer_class = AdminPanelUserSerializer
    allowed_methods = ['GET', 'PATCH', 'DELETE']

    def delete(self, request, *args, **kwargs):
        user = self.get_object()
        if user.driving_school:
            return ErrorResponse(
                message='User is associated with driving school')

        user.delete()
        return SuccessResponse("User deleted successfully")

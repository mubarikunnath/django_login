from coreapi import Field
from rest_framework.filters import BaseFilterBackend


class AdminPanelUserFilterBackend(BaseFilterBackend):
    def get_schema_fields(self, view):
        return [
            Field(
                name='user_type',
                location='query',
                required=False,
                type='string'
            ),
        ]

    def filter_queryset(self, request, queryset, view):
        user_type = request.query_params.get('user_type', None)

        if user_type:
            queryset = queryset.filter(user_type=user_type)
        return queryset

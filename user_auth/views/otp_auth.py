from rest_framework.generics import (
    CreateAPIView,
    UpdateAPIView,
)
from user_auth.serializers import (
    OTPSendSerializer,
    LoginOTPVerifySerializer,
    UserBasicRegistrationSerializer
)
from drivezy.response import SuccessResponse, ErrorResponse
from user_auth.permission import IsAuthenticatedUser


class OTPSendView(CreateAPIView):
    serializer_class = OTPSendSerializer

    def post(self, request):
        otp_serializer = OTPSendSerializer(data=request.data)
        if otp_serializer.is_valid():
            return SuccessResponse(data=otp_serializer.validated_data,
                                   message="OTP sent successfully")

        message = "Error"
        if otp_serializer.errors.get('non_field_errors'):
            message = otp_serializer.errors["non_field_errors"][0]
        return ErrorResponse(message=message)


class OTPVerifyView(CreateAPIView):
    serializer_class = LoginOTPVerifySerializer

    def post(self, request):
        token_serializer = LoginOTPVerifySerializer(data=request.data)
        if token_serializer.is_valid():
            return SuccessResponse(data=token_serializer.validated_data,
                                   message="Login successful")

        message = 'Verification failed, Please resend and try again or '\
            'contact customer care.'
        if token_serializer.errors.get('non_field_errors'):
            message = str(token_serializer.errors["non_field_errors"][0])
        return ErrorResponse(message=message)


class BasicRegistrationView(UpdateAPIView):
    permission_classes = (IsAuthenticatedUser,)
    serializer_class = UserBasicRegistrationSerializer
    allowed_methods = ('PUT',)

    def get_object(self):
        return self.request.user

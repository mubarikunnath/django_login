
from constants import OTP_EXPIRY_MINUTES
import logging
from user_auth.models import User
from django.core.cache import cache
from user_auth.utilities import send_otp
from datetime import timedelta
from rest_framework import serializers
from django.utils import timezone
from user_auth.serializers import UserSerializer
from .auth import UserJWTSerializer


error_logger = logging.getLogger('error_logger')


class OTPSendSerializer(serializers.Serializer):
    mobile = serializers.CharField(required=True)

    def validate(self, attrs):
        mobile = attrs.get('mobile')
        if not mobile.isnumeric():
            message = 'Please enter a valid mobile number.'
            raise serializers.ValidationError(message)
        user, _ = User.objects.get_or_create(
            username=mobile, mobile=mobile, user_type=5)
        status = send_otp(user)
        if not status:
            message = 'Could not send OTP, Please try again or ' +\
                'contact customer care.'
            raise serializers.ValidationError(message)
        return {'otp_sent': True, 'mobile': mobile}


class LoginOTPVerifySerializer(serializers.Serializer):
    mobile = serializers.CharField(write_only=True)
    otp = serializers.CharField(max_length=4, write_only=True)
    '''
    # DISABLED: Uncomment after adding notification module
    device_id = serializers.CharField(max_length=128, write_only=True,
                                      required=False, allow_null=True)
    fcm_token = serializers.CharField(max_length=256, write_only=True,
                                      required=False, allow_null=True)
    '''

    def validate(self, attrs):
        expiry_time = timezone.now() - timedelta(minutes=OTP_EXPIRY_MINUTES)
        credentials = {
            'mobile': attrs.get('mobile'),
            'otp': int(attrs.get('otp'))
        }
        if not all(credentials.values()):
            message = 'Must include mobile number and OTP'
            raise serializers.ValidationError(message)
        if not credentials['mobile'].isnumeric():
            message = 'Please enter a valid mobile number.'
            raise serializers.ValidationError(message)
        try:
            user = User.objects.get(username=credentials['mobile'])
        except User.DoesNotExist:
            message = 'This mobile number not registered on our system,' \
                ' Please contact customer care.'
            raise serializers.ValidationError(message)
        if user.otp != credentials['otp']:
            message = 'Invalid OTP or OTP not generated.'
            raise serializers.ValidationError(message)

        if expiry_time > user.otp_sent_at:
            message = 'OTP Expired, Please resend to continue.'
            raise serializers.ValidationError(message)
        user.otp = None
        user.otp_sent_at = None
        user.last_login = timezone.now()
        fields_updated = ['otp', 'otp_sent_at', 'last_login']
        if not user.mobile_verified:
            user.mobile_verified = True
            fields_updated.append('mobile_verified')
        user.save(update_fields=fields_updated)
        token = UserJWTSerializer.get_token(user)
        acc = token.access_token
        cache.set(acc['jti'], user.id, timeout=1728000)
        user_data = {}
        if user.basic_registration:
            user_data = UserSerializer(user).data
        '''
        # DISABLED: Uncomment after adding notification module
        fcm = attrs.get("fcm_token")
        device_id = attrs.get("device_id")
        if fcm:
            device, _ = GCMDevice.objects.get_or_create(
                registration_id=fcm, device_id=device_id,
                cloud_message_type="FCM")
            device.user = user
            device.save()
        '''
        data = {'refresh': str(token), 'access': str(acc), 'user': user_data,
                'basic_registration': user.basic_registration}
        return data


class UserBasicRegistrationSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'name', 'profile_photo', 'email')

    def update(self, instance, validated_data):
        instance = super().update(instance, validated_data)
        instance.basic_registration = True
        instance.save(update_fields=['basic_registration'])
        return instance

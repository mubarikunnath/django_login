from django.urls import path

from rest_framework_simplejwt.views import TokenVerifyView

from user_auth.views import (
    LoginView,
    PasswordResetView,
    LogoutView,
    ProfileView,
    TokenRefreshView,
    OTPSendView,
    OTPVerifyView,
    BasicRegistrationView
)

urlpatterns = [
    path('login/', LoginView.as_view(), name='login-view'),
    path('refresh/', TokenRefreshView.as_view(),
         name='token_refresh'),
    path('password/reset/', PasswordResetView.as_view(),
         name='reset-password'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('token/verify/', TokenVerifyView.as_view(), name='token_verify'),
    path('profile/', ProfileView.as_view(), name='profile'),

    path('otp/send/', OTPSendView.as_view(), name='otp-send'),
    path('otp/verify/', OTPVerifyView.as_view(), name='otp-verify'),
    path('basic/registration/', BasicRegistrationView.as_view(),
         name='basic-registration'),


]

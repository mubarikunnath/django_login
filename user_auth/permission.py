from rest_framework.permissions import (
    IsAuthenticated,
    BasePermission

)
from rest_framework import status
from rest_framework.generics import (
    ListCreateAPIView,
    CreateAPIView,
    RetrieveAPIView,
    RetrieveUpdateAPIView,
    RetrieveUpdateDestroyAPIView,
    DestroyAPIView,
    ListAPIView,
    UpdateAPIView,
    RetrieveDestroyAPIView

)
from django.core.cache import cache
from django.conf import settings
import jwt
from drivezy.response import ErrorResponse


class IsAuthenticatedUser(IsAuthenticated):
    def has_permission(self, request, view):
        if not request.user.is_anonymous and \
            not request.user.is_deleted_user and \
                super(IsAuthenticatedUser, self).has_permission(request, view):

            jwt_secret = settings.SIMPLE_JWT.get('SIGNING_KEY')
            token = request.META.get("HTTP_AUTHORIZATION").split()[1]
            jti = jwt.decode(token, jwt_secret,
                             algorithms=["HS256"])['jti']
            if cache.get(jti):
                return True
            return False

        return False


class IsAdmin(BasePermission):
    def has_permission(self, request, view):
        if request.user.user_type == 1:
            return True
        return False


class IsOwnerOrAdmin(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.user.user_type in [2, 3]:
            return True
        return False


class CustomCreateAPIView(CreateAPIView):
    def post(self, request, *args, **kwargs):
        if not request.user.driving_school.subscriptions.filter(
                is_active=True).exists():
            return ErrorResponse(message="upgrade required",
                                 status_code=status.HTTP_426_UPGRADE_REQUIRED)

        return super().post(request, *args, **kwargs)


class CustomListAPIView(ListAPIView):

    def get(self, request, *args, **kwargs):
        if not request.user.driving_school.subscriptions.filter(
                is_active=True).exists():
            return ErrorResponse(message="upgrade required",
                                 status_code=status.HTTP_426_UPGRADE_REQUIRED)
        return super().get(request, *args, **kwargs)


class CustomRetrieveAPIView(RetrieveAPIView):

    def get(self, request, *args, **kwargs):
        if not request.user.driving_school.subscriptions.filter(
                is_active=True).exists():
            return ErrorResponse(message="upgrade required",
                                 status_code=status.HTTP_426_UPGRADE_REQUIRED)
        return super().get(request, *args, **kwargs)


class CustomDestroyAPIView(DestroyAPIView):

    def delete(self, request, *args, **kwargs):
        if not request.user.driving_school.subscriptions.filter(
                is_active=True).exists():
            return ErrorResponse(message="upgrade required",
                                 status_code=status.HTTP_426_UPGRADE_REQUIRED)
        return super().delete(request, *args, **kwargs)


class CustomUpdateAPIView(UpdateAPIView):

    def put(self, request, *args, **kwargs):
        if not request.user.driving_school.subscriptions.filter(
                is_active=True).exists():
            return ErrorResponse(message="upgrade required",
                                 status_code=status.HTTP_426_UPGRADE_REQUIRED)
        return super().put(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        if not request.user.driving_school.subscriptions.filter(
                is_active=True).exists():
            return ErrorResponse(message="upgrade required",
                                 status_code=status.HTTP_426_UPGRADE_REQUIRED)
        return super().patch(request, *args, **kwargs)


class CustomListCreateAPIView(ListCreateAPIView):

    def get(self, request, *args, **kwargs):
        if not request.user.driving_school.subscriptions.filter(
                is_active=True).exists():
            return ErrorResponse(message="upgrade required",
                                 status_code=status.HTTP_426_UPGRADE_REQUIRED)
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if not request.user.driving_school.subscriptions.filter(
                is_active=True).exists():
            return ErrorResponse(message="upgrade required",
                                 status_code=status.HTTP_426_UPGRADE_REQUIRED)
        return super().post(request, *args, **kwargs)


class CustomRetrieveUpdateAPIView(RetrieveUpdateAPIView):

    def get(self, request, *args, **kwargs):
        if not request.user.driving_school.subscriptions.filter(
                is_active=True).exists():
            return ErrorResponse(message="upgrade required",
                                 status_code=status.HTTP_426_UPGRADE_REQUIRED)
        return super().get(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        if not request.user.driving_school.subscriptions.filter(
                is_active=True).exists():
            return ErrorResponse(message="upgrade required",
                                 status_code=status.HTTP_426_UPGRADE_REQUIRED)
        return super().put(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        if not request.user.driving_school.subscriptions.filter(
                is_active=True).exists():
            return ErrorResponse(message="upgrade required",
                                 status_code=status.HTTP_426_UPGRADE_REQUIRED)
        return super().patch(request, *args, **kwargs)


class CustomRetrieveDestroyAPIView(RetrieveDestroyAPIView):

    def get(self, request, *args, **kwargs):
        if not request.user.driving_school.subscriptions.filter(
                is_active=True).exists():
            return ErrorResponse(message="upgrade required",
                                 status_code=status.HTTP_426_UPGRADE_REQUIRED)
        return super().get(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        if not request.user.driving_school.subscriptions.filter(
                is_active=True).exists():
            return ErrorResponse(message="upgrade required",
                                 status_code=status.HTTP_426_UPGRADE_REQUIRED)
        return super().delete(request, *args, **kwargs)


class CustomRetrieveUpdateDestroyAPIView(RetrieveUpdateDestroyAPIView):

    def get(self, request, *args, **kwargs):
        if not request.user.driving_school.subscriptions.filter(
                is_active=True).exists():
            return ErrorResponse(message="upgrade required",
                                 status_code=status.HTTP_426_UPGRADE_REQUIRED)
        return super().get(request, *args, **kwargs)

    def put(self, request, *args, **kwargs):
        if not request.user.driving_school.subscriptions.filter(
                is_active=True).exists():
            return ErrorResponse(message="upgrade required",
                                 status_code=status.HTTP_426_UPGRADE_REQUIRED)
        return super().put(request, *args, **kwargs)

    def patch(self, request, *args, **kwargs):
        if not request.user.driving_school.subscriptions.filter(
                is_active=True).exists():
            return ErrorResponse(message="upgrade required",
                                 status_code=status.HTTP_426_UPGRADE_REQUIRED)
        return super().patch(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        if not request.user.driving_school.subscriptions.filter(
                is_active=True).exists():
            return ErrorResponse(message="upgrade required",
                                 status_code=status.HTTP_426_UPGRADE_REQUIRED)
        return super().delete(request, *args, **kwargs)

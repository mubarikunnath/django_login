

import logging
import random
import requests

from django.utils import timezone

from drivezy.settings import DIGIMILES_USERNAME, DIGIMILES_PASSWORD, \
    DIGIMILES_SOURCE, DIGIMILES_ENTITY_ID, DIGIMILES_TEMPLATE_ID, \
    DIGIMILES_LOGIN_URL, DIGIMILES_OTP_TEMPLATE, ENVIRONMENT

error_logger = logging.getLogger('error_logger')


def build_message_data(mobile_number, message):
    url = f"{DIGIMILES_LOGIN_URL}?" +\
        f"username=di78-{DIGIMILES_USERNAME}&password={DIGIMILES_PASSWORD}&" +\
        f"type=0&dlr=1&destination={mobile_number}&" +\
        f"source={DIGIMILES_SOURCE}&message={message}&" +\
        f"entityid={DIGIMILES_ENTITY_ID}&tempid={DIGIMILES_TEMPLATE_ID}"
    return url


def set_test_otp(user):
    user.otp = 1234
    user.otp_sent_at = timezone.now()
    user.save(update_fields=['otp', 'otp_sent_at'])
    return True


def send_otp(user):
    status = False
    if ENVIRONMENT in ('local', 'dev'):
        set_test_otp(user)
        return True
    try:
        otp = random.randint(1000, 9999)
        message = DIGIMILES_OTP_TEMPLATE % str(otp)
        mobile_number = user.mobile
        url = build_message_data(mobile_number, message)
        response = requests.post(url=url)
        error_logger.info(response.content)
        if response.ok:
            user.otp = otp
            user.otp_sent_at = timezone.now()
            user.save(update_fields=['otp', 'otp_sent_at'])
            status = True
    except Exception as e:
        error_logger.error(e)
    return status

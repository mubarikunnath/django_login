from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from user_auth.models import User, StaffAttendance


class UserAdmin(UserAdmin):
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'name', 'mobile', 'user_type',
                       'email', 'password1', 'password2',),
        }),
    )
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (('Personal info'),
         {'fields': (
             'name', 'email', 'mobile', 'profile_photo', 'otp',
             'otp_sent_at', 'driving_school')}),

        (('Permissions'), {
         'fields': ('is_active', 'is_staff', 'is_superuser', 'user_type',
                    'is_deleted_user', )}),
        (('Important dates'), {'fields': ('last_login', 'date_joined',
         'deleted_date')}))

    list_display = ('id', 'name', 'mobile', 'user_type')


admin.site.register(User, UserAdmin)


admin.site.register(StaffAttendance)

from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import UserManager
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

USER_TYPE = (
    (1, 'Admin'),
    (2, 'Driving School Owner'),
    (3, 'Driving School Admin'),
    (4, 'Driving School Staff'),
    (5, 'Student')
)
LEAVE_TYPES = (('casual', 'Casual Leave'), ('sick', 'Sick Leave'), (
    'half_day', 'Half Day Leave'), ('full_day', 'Full Day Leave'),
    ('other', 'Other'))


def validate_mobile(value):
    if not (value.isnumeric() and len(value) >= 10):
        raise ValidationError(
            _(f'{value} is not a valid mobile number'))


class Manager(UserManager):

    def create_superuser(self, username, email, password, **extra_fields):
        extra_fields['is_superuser'] = True
        extra_fields['user_type'] = 1
        super(Manager, self).create_superuser(username, email, password,
                                              **extra_fields)


class User(AbstractUser):
    driving_school = models.ForeignKey(
        'utilities.DrivingSchool',
        on_delete=models.CASCADE, related_name='users', null=True, blank=True)
    name = models.CharField(max_length=200, null=True, blank=True)
    mobile = models.CharField(max_length=12, validators=[validate_mobile])
    designation = models.CharField(max_length=100, null=True, blank=True)
    is_login = models.BooleanField(default=True)
    profile_photo = models.URLField(blank=True, null=True)
    user_type = models.PositiveSmallIntegerField(choices=USER_TYPE)
    is_deleted_user = models.BooleanField(default=False)
    deleted_date = models.DateField(blank=True, null=True)
    email = models.EmailField(_("email address"), null=True, blank=True)
    basic_registration = models.BooleanField(default=False)
    otp = models.IntegerField(null=True, blank=True)
    otp_sent_at = models.DateTimeField(null=True, blank=True)
    mobile_verified = models.BooleanField(default=False)
    email_verified = models.BooleanField(default=False)
    
    objects = Manager()

    def __str__(self):
        return self.username


class StaffAttendance(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='attendances')
    date = models.DateField()
    leave_type = models.CharField(max_length=100, choices=LEAVE_TYPES)
    reason = models.CharField(max_length=100)

    class Meta:
        unique_together = ('user', 'date')

    def __str__(self):
        return self.user.name

import os
from fabric import Connection, Config, task

DEV_HOST_URL = '43.205.157.51'
DEV_HOST_USER = 'ubuntu'
DEV_HOST_PASSWORD = '123456789'


PROJECT_ROOT = '/home/ubuntu/rentpe-backend'
VENV_ACTIVATE = '/home/ubuntu/envs/rentenv/bin/activate'
DEV_BRANCH = 'enfono-dev'


@task
def activate_virtualenv(c):
    with c.run('source ' + VENV_ACTIVATE):
        yield


@task
def install_requirements(c):
    c.run(
        'cd {} && pwd && source {} && pip install -r '
        'requirements.txt'.format(
            PROJECT_ROOT,
            VENV_ACTIVATE))


@task
def run_migrate(c):
    c.run(
        'cd {} && source {} &&'
        'python3 manage.py migrate --noinput'.format(
            PROJECT_ROOT,
            VENV_ACTIVATE))


@task
def run_collect_static(c):
    c.run(
        'cd {} && source {} &&'
        'python3 manage.py collectstatic --noinput'.format(
            PROJECT_ROOT,
            VENV_ACTIVATE))


@task
def restart_all(c):
    c.sudo('supervisorctl reread', pty=True, warn=True)
    c.sudo('supervisorctl update', pty=True, warn=True)
    c.sudo('supervisorctl restart rentpe_api rentpe_celery rentpe_beat', pty=True, warn=True)
    c.sudo('service nginx restart', pty=True, warn=True)


@task
def deploy(context):
    c = Connection(DEV_HOST_URL, user=DEV_HOST_USER,
                   connect_kwargs={'password': DEV_HOST_PASSWORD})
    c.config = Config(
        overrides={'sudo': {'password': DEV_HOST_PASSWORD}})
    # c.run('cd {} && git pull origin {}'.format(
    #     PROJECT_ROOT, DEV_BRANCH))
    # activate_virtualenv(c)
    # install_requirements(c)
    # run_migrate(c)
    # run_collect_static(c)
    # restart_all(c)
    c.run("echo 'ci poli' > cici.txt")
